# frozen_string_literal: true

source 'https://rubygems.org'

gem 'annotate'
gem 'coffee-rails', '~> 4.1.0'
gem 'rails', '4.2.6'
gem 'sass-rails', '~> 5.0'
gem 'sqlite3'
gem 'uglifier', '>= 1.3.0'

gem 'jbuilder', '~> 2.0'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'turbolinks'

gem 'ancestry'
gem 'autoprefixer-rails'
gem 'lorem_ipsum_amet' # random text generators
gem 'slim-rails'
gem 'sprockets-es6'

gem 'bootstrap-sass'

gem 'activeadmin'
gem 'devise'
gem 'paperclip', git: 'git://github.com/thoughtbot/paperclip.git'

gem 'capistrano-passenger', groups: [:development]
gem 'capistrano-rails', groups: [:development]
gem 'capistrano-rvm', groups: [:development]
gem 'figaro'
gem 'recaptcha', require: 'recaptcha/rails'

gem 'mysql2', '~> 0.4.6'
gem 'puma'

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'rubocop', require: false
  gem 'spring'
  gem 'web-console', '~> 2.0'
end
