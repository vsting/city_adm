# frozen_string_literal: true

class CreateMessageContexts < ActiveRecord::Migration
  def change
    create_table :message_contexts do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
