# frozen_string_literal: true

class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
