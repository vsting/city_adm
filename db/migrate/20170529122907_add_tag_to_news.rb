# frozen_string_literal: true

class AddTagToNews < ActiveRecord::Migration
  def change
    add_column :news, :tag, :string
  end
end
