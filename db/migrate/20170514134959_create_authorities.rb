# frozen_string_literal: true

class CreateAuthorities < ActiveRecord::Migration
  def change
    create_table :authorities do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
