# frozen_string_literal: true

class AddColumnsToPostersDescGrayBlockImage < ActiveRecord::Migration
  def change
    add_column :posters, :description, :text
    add_column :posters, :grey_block, :text
    add_column :posters, :image, :text
  end
end
