# frozen_string_literal: true

class AddUniqueToStickers < ActiveRecord::Migration
  def change
    remove_column :stickers, :stickerable_id
    remove_column :stickers, :stickerable_type

    add_column :stickers, :sticker_type, :string
    add_index :stickers, :sticker_type, unique: true
  end
end
