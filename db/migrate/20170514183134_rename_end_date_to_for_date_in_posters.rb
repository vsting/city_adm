# frozen_string_literal: true

class RenameEndDateToForDateInPosters < ActiveRecord::Migration
  def change
    rename_column :posters, :end_date, :for_date
  end
end
