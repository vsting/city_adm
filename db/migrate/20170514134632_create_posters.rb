# frozen_string_literal: true

class CreatePosters < ActiveRecord::Migration
  def change
    create_table :posters do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
