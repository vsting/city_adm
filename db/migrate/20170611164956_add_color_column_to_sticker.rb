# frozen_string_literal: true

class AddColorColumnToSticker < ActiveRecord::Migration
  def change
    add_column :stickers, :color, :string
  end
end
