# frozen_string_literal: true

class AddIndexToExternalContacts < ActiveRecord::Migration
  def change
    add_index :external_contacts, :ancestry
  end
end
