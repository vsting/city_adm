# frozen_string_literal: true

class AddColumnsToAnticurruptsAndInspections < ActiveRecord::Migration
  def change
    add_column :inspections, :partition, :string
    add_column :inspections, :year, :date

    add_column :inspection_ratings, :partition, :string
    add_column :inspection_ratings, :year, :date

    add_column :anticorrupts, :partition, :string
    add_column :anticorrupts, :year, :date
  end
end
