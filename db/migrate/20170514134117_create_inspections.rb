# frozen_string_literal: true

class CreateInspections < ActiveRecord::Migration
  def change
    create_table :inspections do |t|
      t.string :name
      t.string :text

      t.timestamps null: false
    end
  end
end
