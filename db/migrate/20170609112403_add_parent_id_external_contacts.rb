# frozen_string_literal: true

class AddParentIdExternalContacts < ActiveRecord::Migration
  def change
    add_column :external_contacts, :parent_id, :integer
  end
end
