# frozen_string_literal: true

class CreateAnticorrupts < ActiveRecord::Migration
  def change
    create_table :anticorrupts do |t|
      t.string :name
      t.string :text

      t.timestamps null: false
    end
  end
end
