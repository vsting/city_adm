# frozen_string_literal: true

class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
