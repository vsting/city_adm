# frozen_string_literal: true

class CreateNavigatorItems < ActiveRecord::Migration
  def change
    create_table :navigator_items do |t|
      t.string :title
      t.text :points

      t.timestamps null: false
    end
  end
end
