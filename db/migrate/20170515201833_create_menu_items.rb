# frozen_string_literal: true

class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.string :name
      t.string :title

      t.timestamps null: false
    end
  end
end
