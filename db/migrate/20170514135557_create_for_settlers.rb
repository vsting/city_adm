# frozen_string_literal: true

class CreateForSettlers < ActiveRecord::Migration
  def change
    create_table :for_settlers do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
