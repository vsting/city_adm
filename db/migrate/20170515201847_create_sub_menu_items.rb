# frozen_string_literal: true

class CreateSubMenuItems < ActiveRecord::Migration
  def change
    create_table :sub_menu_items do |t|
      t.integer :menu_item_id
      t.string :name
      t.string :title

      t.timestamps null: false
    end
  end
end
