# frozen_string_literal: true

class CreateExternalContacts < ActiveRecord::Migration
  def change
    create_table :external_contacts do |t|
      t.string :name
      t.string :text

      t.timestamps null: false
    end
  end
end
