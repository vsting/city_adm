# frozen_string_literal: true

class AddColumnsToPostersToDate < ActiveRecord::Migration
  def change
    add_column :posters, :end_date, :date
  end
end
