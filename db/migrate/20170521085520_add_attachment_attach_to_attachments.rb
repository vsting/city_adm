# frozen_string_literal: true

class AddAttachmentAttachToAttachments < ActiveRecord::Migration
  def self.up
    change_table :attachments do |t|
      t.attachment :attach
    end
  end

  def self.down
    remove_attachment :attachments, :attach
  end
end
