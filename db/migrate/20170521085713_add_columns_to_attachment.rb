# frozen_string_literal: true

class AddColumnsToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :title, :string
    add_column :attachments, :text, :string
    add_column :attachments, :tag, :string
  end
end
