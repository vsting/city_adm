# frozen_string_literal: true

class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
