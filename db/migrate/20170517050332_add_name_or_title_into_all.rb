# frozen_string_literal: true

class AddNameOrTitleIntoAll < ActiveRecord::Migration
  def change
    add_column :external_contacts, :title, :string
    add_column :events, :name, :string
    add_column :abouts, :name, :string
    add_column :anticorrupts, :title, :string
    add_column :authorities, :name, :string
    add_column :documents, :name, :string
    add_column :for_guests, :name, :string
    add_column :for_settlers, :name, :string
    add_column :galleries, :name, :string
    add_column :histories, :name, :string
    add_column :inspection_ratings, :name, :string
    add_column :inspections, :title, :string
    add_column :menu_items, :text, :text
    add_column :message_contexts, :name, :string
    add_column :navigator_items, :name, :string
    add_column :navigators, :name, :string
    add_column :news, :name, :string
    add_column :officials, :name, :string
    add_column :people, :title, :string
    add_column :posters, :name, :string
    add_column :statistics, :name, :string
    add_column :symbologies, :name, :string
    add_column :vacancies, :name, :string
    add_column :videos, :name, :string
  end
end
