# frozen_string_literal: true

class AddTypeToMenuItem < ActiveRecord::Migration
  def change
    drop_table :officials
    add_column :menu_items, :type, :string
  end
end
