# frozen_string_literal: true

class CreateExternalContactContents < ActiveRecord::Migration
  def change
    create_table :external_contact_contents do |t|
      t.string :title
      t.text :text

      t.timestamps null: false
    end
  end
end
