# frozen_string_literal: true

class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
