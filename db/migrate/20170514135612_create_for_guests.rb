# frozen_string_literal: true

class CreateForGuests < ActiveRecord::Migration
  def change
    create_table :for_guests do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
