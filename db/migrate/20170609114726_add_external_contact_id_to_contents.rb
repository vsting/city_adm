# frozen_string_literal: true

class AddExternalContactIdToContents < ActiveRecord::Migration
  def change
    add_column :external_contact_contents, :external_contact_id, :integer
  end
end
