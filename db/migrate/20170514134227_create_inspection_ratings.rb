# frozen_string_literal: true

class CreateInspectionRatings < ActiveRecord::Migration
  def change
    create_table :inspection_ratings do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
