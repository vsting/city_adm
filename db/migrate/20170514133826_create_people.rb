# frozen_string_literal: true

class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :text

      t.timestamps null: false
    end
  end
end
