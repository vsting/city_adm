# frozen_string_literal: true

class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :title
      t.text :text
      t.string :type

      t.timestamps null: false
    end
  end
end
