# frozen_string_literal: true

class ChangeTextTypeInPosters < ActiveRecord::Migration
  def change
    change_column :posters, :text, :text
  end
end
