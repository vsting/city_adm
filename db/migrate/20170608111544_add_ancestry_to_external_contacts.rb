# frozen_string_literal: true

class AddAncestryToExternalContacts < ActiveRecord::Migration
  def change
    add_column :external_contacts, :ancestry, :string
  end
end
