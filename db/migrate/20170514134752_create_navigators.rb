# frozen_string_literal: true

class CreateNavigators < ActiveRecord::Migration
  def change
    create_table :navigators do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
