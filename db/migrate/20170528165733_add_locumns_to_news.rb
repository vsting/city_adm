# frozen_string_literal: true

class AddLocumnsToNews < ActiveRecord::Migration
  def change
    add_column :news, :description, :text
    add_column :news, :grey_block, :text
    add_column :news, :image, :string
  end
end
