# frozen_string_literal: true

class CreateStickers < ActiveRecord::Migration
  def change
    create_table :stickers do |t|
      t.string :title
      t.text :text
      t.integer :stickerable_id
      t.string :stickerable_type

      t.timestamps null: false
    end
  end
end
