# frozen_string_literal: true

class AddReferencesToStickers < ActiveRecord::Migration
  def change
    add_index :stickers, %w[stickerable_type stickerable_id], name: 'index_stickers_on_stickerable_type_and_stickerable_id'
  end
end
