# frozen_string_literal: true

class CreateSymbologies < ActiveRecord::Migration
  def change
    create_table :symbologies do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
