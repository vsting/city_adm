# frozen_string_literal: true

class AddStatusHashCodeToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :status, :string
    add_column :messages, :hash_code, :string
  end
end
