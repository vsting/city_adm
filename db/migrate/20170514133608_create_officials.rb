# frozen_string_literal: true

class CreateOfficials < ActiveRecord::Migration
  def change
    create_table :officials do |t|
      t.string :title
      t.string :text

      t.timestamps null: false
    end
  end
end
