# frozen_string_literal: true

class AddAuthorIdToPosters < ActiveRecord::Migration
  def change
    add_column :posters, :author_id, :integer
  end
end
