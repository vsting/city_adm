# frozen_string_literal: true

class AddColumnsToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :context, :string
    add_column :documents, :tag, :string
    add_column :documents, :partition, :string
  end
end
