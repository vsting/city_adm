# frozen_string_literal: true

class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :email
      t.string :phone
      t.string :name
      t.string :type
      t.integer :context
      t.text :message

      t.timestamps null: false
    end
  end
end
