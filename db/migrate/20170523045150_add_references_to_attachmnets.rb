# frozen_string_literal: true

class AddReferencesToAttachmnets < ActiveRecord::Migration
  def change
    add_reference :attachments, :attacheable, polymorphic: true, index: true
  end
end
