# frozen_string_literal: true

class AddColumnsToPostersDateAndTime < ActiveRecord::Migration
  def change
    add_column :posters, :date, :string
    add_column :posters, :time, :string
  end
end
