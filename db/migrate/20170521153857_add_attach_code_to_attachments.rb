# frozen_string_literal: true

class AddAttachCodeToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :attach_code, :string
  end
end
