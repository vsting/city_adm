# frozen_string_literal: true

User.find_or_create_by!(email: 'admin@example.com') do |u|
  u.name = 'Vitaly'
  u.password = 'password'
  u.password_confirmation = 'password'
end

MessageContext.create!([{ title: 'Транспорт', text: 'Транспорт' }, { title: 'Недвижимость', text: 'Недвижимость' }, { title: 'ЖКХ', text: 'ЖКХ' }])

2.times do |i1|
  Document.create!(
    title: "title#{i1}",
    text: "text#{i1}",
    name: "name#{i1}",
    context: "context#{i1}",
    tag: 'tag',
    partition: "partition#{i1}"
  )
end

2.times do
  News.create!(
    tag: 'tag',
    title: 'Заголовок новостей',
    text: 'Основной текст новостей',
    name: 'имя модели',
    description: 'описание публикации',
    grey_block: 'текстовая блочная вставка',
    author_id: 1
  )
end

2.times do
  Poster.create!(
    date: '18 may',
    time: '17:18',
    for_date: Date.current,
    title: 'Заголовок Постера',
    text: 'Основной текст Постера',
    name: 'имя модели',
    description: 'описание публикации',
    grey_block: 'текстовая блочная вставка',
    author_id: 1
  )
end
