$(document).ready(() => {
    $('.js-to-top').click(() => {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
    });

    $("A[rel^='prettyPhoto']").prettyPhoto({
        opacity: "0.5",
        deeplinking: false,
        social_tools: false,
        overlay_gallery: false,
        autoplay: false
    });

    //Инициализация галереи слик и поддержание одинаковой высоты слайдов
    class SlickEqualHeightSlider {
        constructor(options) {
            this.elem = options.elem;

            this.elem.on('init breakpoint setPosition', () => {
                this.setSlidesHeight();
            });

            this.elem.slick(options.slickOptions);
        }

        setSlidesHeight() {
            //Делаем все слайды одинаковой высоты
            //https://github.com/kenwheeler/slick/issues/179#issuecomment-204768021
            let slides = this.elem.find('.slick-slide');
            slides.height('auto');
            let slickTrackHeight = this.elem.find('.slick-track').height();
            slides.css('height', slickTrackHeight + 'px');
        }
    }

    $('.js-affiche-slider').each((index, elem) => {
        new SlickEqualHeightSlider({
            elem: $(elem),
            slickOptions: {
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            }
        });
    });

    $('.js-nav-slider').each((index, elem) => {
        new SlickEqualHeightSlider({
            elem: $(elem),
            slickOptions: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false
            }
        })
    });
});