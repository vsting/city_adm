$(document).ready(() => {
    class Message {
        constructor(options) {
            this.elem = options.elem;
            this.formElem = this.elem.find('form.message');
            this.formElem.ajaxForm(this.processFormSend.bind(this));
        }

        processFormSend() {
            this.formElem.slideUp();
            //Начинаем показывать текст чуть раньше чем форма скроется
            //чтобы не было дерганий
            setTimeout(() => {
                this.elem.find('.thanx').fadeIn(600);
            }, 200)
        }
    }

    $('.page_message').each((index, elem) => {
        new Message({
            elem: $(elem)
        });
    });
});