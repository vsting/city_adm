$(document).ready(() => {
    $('.js-welcome-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 577,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    $('.js-governance-slider').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 577,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 374,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    function setHeight() {
        $('.js-tabs.__js-with-slider .js-tabs-contents').each(function () {
            let h = 0;

            $(this).children().each(function () {
                if (h < $(this).height()) {
                    h = $(this).height();
                }
            });

            $(this).css('min-height', `${h}px`);
        });
    }

    setHeight();
    $(window).resize(setHeight);

    class NewsSlider {
        constructor(options) {
            this.elem = options.elem;
            this.slickParams = {
                slidesToShow: 4,
                slidesToScroll: 4,
                dots: true,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    }
                ]
            };

            $(window).on('resize', this.checkSlider.bind(this));
            $(window).trigger('resize');
        }

        checkSlider() {
            //На мобайле галерею выключаем
            if(document.documentElement.clientWidth < 577 && this.elem.is('.slick-initialized')) {
                this.elem.slick('unslick');
            }

            if(document.documentElement.clientWidth >= 577 && !this.elem.is('.slick-initialized')) {
                this.elem.slick(this.slickParams);
            }
         }
    }



    class SectionMap {
        constructor(options) {
            this.elem = options.elem;
            this.tabElems = this.elem.find('.tabs span');
            this.tabContentElems = this.elem.find('.tabs-content .item');

            this.elem.click((event) => {
                let tab = $(event.target).closest('.tabs span:not(.__active)');
                if(tab.length && this.elem[0].contains(event.target)) {
                    this.setTab(tab);
                }
            });
        }

        setTab(tab) {
            this.tabElems.removeClass('__active');
            tab.addClass('__active');
            this.tabContentElems.hide();
            this.tabContentElems.filter(tab.data('target'))
                .show()
                .find('.map-component')
                .trigger('map:reload');
        }
    }

    $('section.map-section').each((index, elem) => {
        new SectionMap({
            elem: $(elem)
        });
    });

    $('.js-news-slider').each((index, elem) => {
        new NewsSlider({
            elem: $(elem)
        });
    });
});