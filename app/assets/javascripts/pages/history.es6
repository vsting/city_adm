$(document).ready(() => {
    class HistoryPage {
        constructor(options) {
            this.elem = options.elem;
            this.elem.find('.inset-with-gal .gallery').slick();
        }
    }

    $('.page_history').each((index, elem) => {
        new HistoryPage({
            elem: $(elem)
        });
    });
});