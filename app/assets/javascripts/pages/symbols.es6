$(document).ready(() => {
    class SymbolsPage {
        constructor(options) {
            this.elem = options.elem;
            this.elem.find('.gallery-container .gallery')
                .slick({
                    dots: true,
                    arrows: false
                });
        }
    }

    $('.page_symbols').each((index, elem) => {
        new SymbolsPage({
            elem: $(elem)
        });
    });
});