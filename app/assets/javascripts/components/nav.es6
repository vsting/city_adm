$(document).ready(() => {
    class MainNav {
        constructor(options) {
            this.el = options.el;
            this.fullnavElem = $('body').find('.fullnav');
            this.fullnavOpenButton = this.el.find('.fullnav-button');
            this.fullnavCloseButton = this.fullnavElem.find('.close');
            this.subscribeForm = this.el.find('.nav-controls-top form');

            this.controls = this.el.find('.js-nav-controls');
            this.toggleBtn = this.el.find('.js-nav-controls-toggle');
            this.items = this.el.find('.js-nav-item');

            this.controlsIsShow = false;
            this.navIsFixed = false;

            this.fullnavCloseButton.click(this.hideFullnav.bind(this));
            this.fullnavOpenButton.click(this.showFullnav.bind(this));
            this.toggleBtn.click(this.toggleControls.bind(this));
            this.items.click(this.toggleMenuItem.bind(this));

            this.el.click((event) => {
               if($(event.target).closest('.subscribe-link').length
               && this.el[0].contains(event.target)) {
                   event.preventDefault();
                   this.showSubscribeForm();
               }
            });

            this.subscribeForm.ajaxForm(this.processSubscribeFormSubmit.bind(this));

            $(window).click(this.checkClickOut.bind(this));
            $(window).on('scroll resize', this.setNavState.bind(this));

            this.setNavState();
        }

        processSubscribeFormSubmit() {
            this.subscribeForm.hide();
            this.el.find('.nav-controls-top .thanx').css('display', 'inline-block');
        }

        showSubscribeForm() {
            this.el.find('.subscribe-link').hide();
            this.subscribeForm.css('display', 'inline-block');
            this.subscribeForm.find('input[type=email]').focus();
        }

        showFullnav() {
            this.fullnavElem.show();
        }

        hideFullnav() {
            this.fullnavElem.hide();
        }

        toggleControls() {
            if (this.controlsIsShow) {
                this.hideControls();
                return;
            }

            this.showControls();
        }

        hideControls() {
            this.controlsIsShow = false;
            this.controls.removeClass('__show');
            this.items.removeClass('__active');
        }

        showControls() {
            this.controls.addClass('__show');
            this.controlsIsShow = true;
        }

        toggleMenuItem(e) {
            let item = $(e.target).closest('.js-nav-item-title').parent('.js-nav-item');
            if (!item.length) {
                return;
            }

            if (item.hasClass('__active')) {
                this.hideMenuItem(item);
            } else {
                this.showMenuItem(item);
            }
        }

        showMenuItem(el) {
            if($(window).width() >= 1200) {
                this.items.removeClass('__active');
            }

            el.addClass('__active');
        }

        hideMenuItem(el) {
            el.removeClass('__active');
        }

        checkClickOut(e) {
            if ($(e.target).closest(this.el).length) {
                return;
            }

            this.hideControls();
        }

        setNavState() {
            if ($(window).width() < 1200) {
                if (this.navIsFixed) {
                    return;
                }

                this.el.addClass('__fixed');
                this.navIsFixed = true;
            } else if (!this.navIsFixed && $(window).scrollTop() > this.el.outerHeight()) {
                this.el.addClass('__fixed');
                this.navIsFixed = true
            } else if (this.navIsFixed && $(window).scrollTop() < this.el.outerHeight()) {
                this.el.removeClass('__fixed');
                this.navIsFixed = false;
            }
        }
    }

    $('.js-main-nav').each((index, elem) => {
        new MainNav({
            el: $(elem)
        });
    });
});
