class ParamsComponent {
    constructor(options) {
        this.elem = options.elem;
        this.searchFormElem = this.elem.find('.ct-ps-search');

        this.elem.click((event) => {
           if($(event.target).closest('.ct-ps-icon').length
               && this.elem[0].contains(event.target)) {
               this.toggleForm();
           }
        });
    }

    toggleForm() {
        if(this.searchFormElem.is(':visible')) {
            this.searchFormElem.hide();
            this.searchFormElem.find('input[type=text]').blur();
            return;
        }

        this.searchFormElem.show();
        this.searchFormElem.find('input[type=text]').focus();
    }
}

$(document).ready(() => {
    $('.params-component').each((index, elem) => {
      new ParamsComponent({
          elem: $(elem)
      });
    });
});