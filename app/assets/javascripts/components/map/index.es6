class MapComponent {
    constructor(options) {
        this.elem = options.elem;
        this.mapElem = this.elem.find('.ct-mc-map');
        this.objects = this.elem.data('objects');
        this.collectionData = this.elem.data('collection');
        this.collections = {};

        ymaps.ready(this.initMap.bind(this));

        this.elem.click((event) => {
            let menuPoint = $(event.target).closest('.ct-mc-menu-item');
            if (menuPoint.length && this.elem[0].contains(event.target)) {
                this.toggleCategory(menuPoint);
            }
        });

        this.elem.on('map:reload', () => {
            if (!this.map) {
                return;
            }

            this.map.container.fitToViewport();
        });
    }

    toggleCategory(menuPoint) {
        if(!this.collections[menuPoint.data('id')]) {
            return;
        }

        if (menuPoint.hasClass('__inactive')) {
            menuPoint.removeClass('__inactive');
            this.collections[menuPoint.data('id')].options.set('visible', true);
            return;
        }

        menuPoint.addClass('__inactive');
        this.collections[menuPoint.data('id')].options.set('visible', false);
    }

    initMap() {
        this.map = new ymaps.Map(this.mapElem[0], {
            center: this.elem.data('center') || [57.4605, 41.5123],
            zoom: this.elem.data('zoom') || 17,
            controls: ['fullscreenControl', 'zoomControl']
        });
        this.map.behaviors.disable('scrollZoom');

        for (let category of this.collectionData) {
            let collection = new ymaps.GeoObjectCollection({}, {});

            for (let object of category.objects) {
                collection.add(new ymaps.Placemark(object.coords, {
                        balloonContentHeader: object.name,
                        balloonContentBody: object.description
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: category.iconUrl,
                        iconImageSize: [52, 62],
                        iconImageOffset: [-26, -62]
                    }
                ));
            }

            this.map.geoObjects.add(collection);
            this.collections[category.id] = collection;
        }

        //Если есть изначально неактивные категории
        this.elem.find('.ct-mc-menu-item.__inactive').each((index, elem) => {
            let id = $(elem).data('id');
            if(!id) {
                return;
            }

            this.collections[id].options.set('visible', false);
        });
    }
}

$(document).ready(() => {
    $('.map-component').each((index, elem) => {
        new MapComponent({
            elem: $(elem)
        });
    });
});
