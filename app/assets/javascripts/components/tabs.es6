$(document).ready(() => {
    class TabsComponent {
        constructor(options) {
            let obj = this;
            this.elem = options.elem;
            this.controls = this.elem.find('.js-tabs-controls').children();
            this.contents = this.elem.find('.js-tabs-contents').children();

            this.controls.click(function() {
                obj.switchTab($(this));
            });
        }

        switchTab(el) {
            let target = this.contents.filter(`[data-id=${el.data('id')}]`);
            this.controls.removeClass('__active');
            this.contents.removeClass('__active');

            el.addClass('__active');
            target.addClass('__active');
        }
    }

    $('.js-tabs').each((index, elem) => {
        new TabsComponent({
            elem: $(elem)
        });
    });
});