class DropdownComponent {
    constructor(options) {
        this.elem = options.elem;
        if(this.elem.hasClass('__select')) {
            this.select = true;
        }

        //Тоглим ДД
        this.elem.click((event) => {
            if($(event.target).closest('.ct-dc-trigger').length &&
            this.elem[0].contains(event.target)) {
                this.toggleDropdown();
            }
        });

        //Если кликнули вне компонента, то прячем ДД
        $(document).click((event) => {
            if (this.elem[0].contains(event.target)) {
                return;
            }

            this.hideDropdown();
        });

        //Выбор элемента
        if(this.select) {
            this.elem.click((event) => {
               let selectedElem = $(event.target).closest('li');
               if(selectedElem.length && this.elem[0].contains(event.target)) {
                   event.preventDefault();
                   this.selectElem(selectedElem);
               }
            });
        }
    }

    selectElem(selectedElem) {
        this.elem.find('input[type=hidden]')
            .val(selectedElem.data('value'));
        this.elem.find('.ct-dc-trigger span')
            .text(selectedElem.text());
        this.hideDropdown();
    }

    toggleDropdown() {
        if(this.elem.hasClass('__active')) {
            this.hideDropdown();
            return;
        }

        this.showDropdown();
    }

    hideDropdown() {
        this.elem.removeClass('__active');
    }

    showDropdown() {
        this.elem.addClass('__active');
    }
}

$(document).ready(() => {
    $('.dropdown-component').each((index, elem) => {
        new DropdownComponent({
           elem: $(elem)
        });
    });
});
