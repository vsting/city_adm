$(document).ready(() => {
    $('.js-move-to').click(function () {
        let target = $(this).data('target');

        console.log('click');

        $('html, body').animate({
            scrollTop: $(`.${target}`).offset().top
        }, 800);
    });
});