# frozen_string_literal: true

class ExternalContactContent < ActiveRecord::Base
  belongs_to :external_contact
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments
end

# == Schema Information
#
# Table name: external_contact_contents
#
#  created_at          :datetime         not null
#  external_contact_id :integer
#  id                  :integer          not null, primary key
#  text                :text(65535)
#  title               :string(255)
#  updated_at          :datetime         not null
#
