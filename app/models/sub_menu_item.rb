# frozen_string_literal: true

class SubMenuItem < ActiveRecord::Base
end

# == Schema Information
#
# Table name: sub_menu_items
#
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  menu_item_id :integer
#  name         :string(255)
#  title        :string(255)
#  updated_at   :datetime         not null
#
