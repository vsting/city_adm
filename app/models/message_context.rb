# frozen_string_literal: true

class MessageContext < ActiveRecord::Base
end

# == Schema Information
#
# Table name: message_contexts
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
