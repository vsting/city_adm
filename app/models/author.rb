# frozen_string_literal: true

class Author < ActiveRecord::Base
  belongs_to :user, dependent: :destroy
  has_many :news
  has_many :poster
end

# == Schema Information
#
# Table name: authors
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  updated_at :datetime         not null
#  user_id    :integer
#
