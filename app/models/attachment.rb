# frozen_string_literal: true

class Attachment < ActiveRecord::Base
  belongs_to :attacheable, polymorphic: true

  attr_accessor :attach, :attacheable_type
  has_attached_file :attach, styles: ->(a) { a.instance.check_file_type },
                             url: '/shared/:id/:style/:basename.:extension', # lambda { |a| a.instance.check_file_type_for_url },
                             path: ':rails_root/public/shared/:id/:style/:basename.:extension' # lambda { |a| a.instance.check_file_type_for_path }

  validates_attachment_file_name :attach,
                                 matches: [
                                   /\.pdf$/i,
                                   /\.docx?$/i,
                                   /\.doc?$/i,
                                   /\.xlsx?$/i,
                                   /\.xls?$/i,
                                   /\.odt?$/i,
                                   /\.ods?$/i,
                                   /\.jpg?$/i,
                                   /\.jpeg?$/i,
                                   /\.png?$/i,
                                   /\.txt?$/i,
                                   /^\.js?$/i,
                                   /^\.exe?$/i,
                                   /^\.sh?$/i,
                                   /^\.bat?$/i,
                                   /^\.com?$/i
                                 ]
  do_not_validate_attachment_file_type :attach

  #   validates_attachment :attach, content_type: {
  #     content_type: [
  #       'multipart/form-data',
  #       'image/png',
  #       'image/jpeg',
  #       'image/gif',
  #       'image/jpg',
  #       'image/bmp',
  #       'text/plain',
  #       'text/xls',
  #       'application/pdf',
  #       'application/zip',
  #       'application/msword',
  #       'application/msexcel',
  #       'application/excel',
  #       'application/xls',
  #       'application/vnd.ms-office',
  #       'application/vnd.ms-excel',
  #       'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  #       'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  #       'application/x-msdownload'
  #     ]
  #   }

  before_destroy :clear_dir

  def clear_dir
    file_path = Rails.root.join("public/shared/#{id}")
    FileUtils.rm_rf(file_path) if Dir.exist?(file_path)
  end

  def check_file_type
    if is_image_type?
      {
        news_item: '250x250',
        author_thumb25: '25x25',
        author_thumb60: '60x60',
        medium: '238x238>',
        thumb: '100x100>',
        afishe_item: '500x500',
        afishe_one: '550x350',
        gallery_item01: '250x250'
      }
    else
      {}
    end
  end

  def check_file_type_for_path
    is_image_type? ? ':rails_root/public/shared/:id/:style/:basename.:extension' : ':rails_root/public/shared/:id/:basename.:extension'
  end

  def check_file_type_for_url
    is_image_type? ? '/shared/:id/:style/:basename.:extension' : '/shared/:id/:basename.:extension'
  end

  def is_image_type?
    attach_content_type =~ /image/
  end

  def is_not_avatar?; end

  def short_title
    attach_file_name.length > 46 ? attach_file_name[0..46].ljust(50, '.') : attach_file_name
  end

  def title
    attach_file_name
  end

  def file_ext
    attach_file_name.split('.').last
  end

  def title_with_out_ext
    File.basename(attach.path, File.extname(attach.path))
  end
end

# == Schema Information
#
# Table name: attachments
#
#  attach_code         :string(255)
#  attach_content_type :string(255)
#  attach_file_name    :string(255)
#  attach_file_size    :integer
#  attach_updated_at   :datetime
#  attacheable_id      :integer
#  attacheable_type    :string(255)
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  name                :string(255)
#  tag                 :string(255)
#  text                :string(255)
#  title               :string(255)
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_attachments_on_attacheable_type_and_attacheable_id  (attacheable_type,attacheable_id)
#
