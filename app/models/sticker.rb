# frozen_string_literal: true

class Sticker < ActiveRecord::Base
  # belongs_to :stickerable, polymorphic: true
  validates :sticker_type, presence: true, uniqueness: true

  def parent_instance
    sticker_type.constantize.new
  end
end

# == Schema Information
#
# Table name: stickers
#
#  color        :string(255)
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  sticker_type :string(255)
#  text         :text(65535)
#  title        :string(255)
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_stickers_on_sticker_type  (sticker_type) UNIQUE
#
