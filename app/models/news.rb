# frozen_string_literal: true

class News < ActiveRecord::Base
  belongs_to :author
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments

  # attr_accessor :attachments
end

# == Schema Information
#
# Table name: news
#
#  author_id   :integer
#  created_at  :datetime         not null
#  description :text(65535)
#  grey_block  :text(65535)
#  id          :integer          not null, primary key
#  image       :string(255)
#  name        :string(255)
#  tag         :string(255)
#  text        :string(255)
#  title       :string(255)
#  updated_at  :datetime         not null
#
