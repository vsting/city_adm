# frozen_string_literal: true

class Document < ActiveRecord::Base
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments
end

# == Schema Information
#
# Table name: documents
#
#  context    :string(255)
#  created_at :datetime         not null
#  file_type  :string(255)
#  id         :integer          not null, primary key
#  name       :string(255)
#  partition  :string(255)
#  tag        :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
