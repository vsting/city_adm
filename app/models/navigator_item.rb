# frozen_string_literal: true

class NavigatorItem < ActiveRecord::Base
  belongs_to :navigator
end

# == Schema Information
#
# Table name: navigator_items
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  points     :text(65535)
#  title      :string(255)
#  updated_at :datetime         not null
#
