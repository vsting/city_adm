# frozen_string_literal: true

class Inspection < ActiveRecord::Base
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments
end

# == Schema Information
#
# Table name: inspections
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  partition  :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#  year       :date
#
