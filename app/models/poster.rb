# frozen_string_literal: true

class Poster < ActiveRecord::Base
  belongs_to :author
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments
end

# == Schema Information
#
# Table name: posters
#
#  author_id   :integer
#  created_at  :datetime         not null
#  date        :string(255)
#  description :text(65535)
#  for_date    :date
#  grey_block  :text(65535)
#  id          :integer          not null, primary key
#  image       :text(65535)
#  name        :string(255)
#  text        :text(65535)
#  time        :string(255)
#  title       :string(255)
#  updated_at  :datetime         not null
#
