# frozen_string_literal: true

class ExternalContact < ActiveRecord::Base
  has_many :attachments, as: :attacheable, dependent: :destroy
  has_many :external_contact_contents, dependent: :destroy
  accepts_nested_attributes_for :attachments
  has_ancestry
end

# == Schema Information
#
# Table name: external_contacts
#
#  ancestry   :string(255)
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  parent_id  :integer
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
# Indexes
#
#  index_external_contacts_on_ancestry  (ancestry)
#
