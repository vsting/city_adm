# frozen_string_literal: true

class Navigator < ActiveRecord::Base
  has_many :navigator_items
  belongs_to :menu_item
end

# == Schema Information
#
# Table name: navigators
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
