# frozen_string_literal: true

class Image < Gallery
end

# == Schema Information
#
# Table name: galleries
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
