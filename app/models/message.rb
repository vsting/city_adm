# frozen_string_literal: true

class Message < ActiveRecord::Base
end

# == Schema Information
#
# Table name: messages
#
#  context    :integer
#  created_at :datetime         not null
#  email      :string(255)
#  hash_code  :string(255)
#  id         :integer          not null, primary key
#  message    :text(65535)
#  name       :string(255)
#  phone      :string(255)
#  status     :string(255)
#  type       :string(255)
#  updated_at :datetime         not null
#
