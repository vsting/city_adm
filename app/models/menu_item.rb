# frozen_string_literal: true

class MenuItem < ActiveRecord::Base
  has_many :messages

  has_many :news
  has_many :authorities
  has_many :navigators

  has_many :galleries
  # has_many :images
  # has_many :videos
  # has_many :posters

  # abouts
  has_many :symbologies
  has_many :histories

  # has_many :officials             #menu
  has_many :inspections
  has_many :inspection_ratings
  has_many :statistics
  has_many :external_contacts
  has_many :anticorrupts
  has_many :people
  has_many :documents
  has_many :vacancies

  has_many :for_guests
  has_many :for_settlers
end

# == Schema Information
#
# Table name: menu_items
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :text(65535)
#  title      :string(255)
#  type       :string(255)
#  updated_at :datetime         not null
#
