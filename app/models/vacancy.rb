# frozen_string_literal: true

class Vacancy < ActiveRecord::Base
  has_many :attachments, as: :attacheable, dependent: :destroy
  accepts_nested_attributes_for :attachments
end


# == Schema Information
#
# Table name: vacancies
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :text(65535)
#  title      :string(255)
#  type       :string(255)
#  updated_at :datetime         not null
#
