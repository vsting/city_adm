# frozen_string_literal: true

class Official < MenuItem # ActiveRecord::Base
end

# == Schema Information
#
# Table name: menu_items
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :text(65535)
#  title      :string(255)
#  type       :string(255)
#  updated_at :datetime         not null
#
