# frozen_string_literal: true

class Authority < ActiveRecord::Base
  belongs_to :menu_item
  has_one :sticker, as: :stickerable, dependent: :destroy
  has_many :attachments, as: :attacheable, dependent: :destroy
end

# == Schema Information
#
# Table name: authorities
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string(255)
#  text       :string(255)
#  title      :string(255)
#  updated_at :datetime         not null
#
