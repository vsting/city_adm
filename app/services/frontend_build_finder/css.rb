# frozen_string_literal: true

class FrontendBuildFinder
  class Css
    def find_file(options)
      method = options[:method] || :relative
      send method, options.except(:method)
    end

    def relative(options)
      File.join('/', 'frontend', options[:namespace], 'build', 'styles', bundle_filename(options[:namespace], "#{options[:filename]}.css"))
    end

    def filesystem(options)
      Rails.root.join('public', 'frontend', options[:namespace], 'build', 'styles', bundle_filename(options[:namespace], "#{options[:filename]}.css"))
    end

    def external(options)
      raise FrontendBuildFinder::NoHostnameGivenError unless options[:hostname]
      File.join(options[:hostname], 'frontend', options[:namespace], 'build', 'styles', bundle_filename(options[:namespace], "#{options[:filename]}.css"))
    end

    private

    def bundle_filename(namespace, styles_file)
      filepath = Rails.root.join('public', 'frontend', namespace, 'build', 'styles', 'styles.json')
      content = FrontendBuildFinder.fetch_content(filepath).fetch(styles_file, nil)
      raise FrontendBuildFinder::GivenEntryNotFoundError unless content
      content
    end
  end
end
