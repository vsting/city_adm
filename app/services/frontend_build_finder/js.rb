# frozen_string_literal: true

class FrontendBuildFinder
  class Js
    def find_file(_options)
      raise NotImplementedError
    end
  end
end
