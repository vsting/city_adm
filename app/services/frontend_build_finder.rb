# frozen_string_literal: true

class FrontendBuildFinder
  def initialize(method, options)
    @method = method
    @options = options
    raise NoNamespaceGivenError unless options[:namespace]
    raise NoFilenameGivenError unless options[:filename]
  end

  def perform
    klass = FrontendBuildFinder.const_get(@method.to_s.capitalize)
    klass.new.find_file(@options)
  end

  def self.fetch_content(path)
    file = File.open(path)
    content = JSON.parse(file.readlines.join)
    file.close
    content
  end

  class NoHostnameGivenError < StandardError; end
  class NoNamespaceGivenError < StandardError; end
  class NoFilenameGivenError < StandardError; end
  class GivenEntryNotFoundError < StandardError; end
end
