# frozen_string_literal: true

class FeedbackMailer < ApplicationMailer
  default from: ENV['MAIL_FROM']

  def feedback(options = {})
    @name = options[:name]
    @surname = options[:surname]
    @email = options[:email]
    @message = options[:message]
    mail(to: ENV['MAIL_TO'], subject: 'Обратная связь с сайта')
  end
end
