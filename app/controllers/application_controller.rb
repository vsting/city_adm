# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :elements
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def elements
    @news_all = News.limit(12)
    @current_user = current_user unless current_user.nil?

    @posters_index = Poster.limit(6) || nil
    @poster_block = @posters_index.first unless @posters_index.nil?
  end
end
