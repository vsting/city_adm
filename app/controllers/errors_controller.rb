# frozen_string_literal: true

class ErrorsController < ApplicationController
  before_action :set_error, only: %i[show edit update destroy]

  def error_404
    render status: 404, formats: [:html] # , :layout => "white", :sub_layout => "left"
  end

  def error_422
    render status: 422, formats: [:html]
  end

  def error_500
    render status: 500, formats: [:html]
  end

  def error_505
    render status: 505, formats: [:html]
  end

  # GET /errors/1
  # GET /errors/1.json
  #   def show
  #     respond_to do |format|
  #       if @error.save
  #         format.html { redirect_to @error, notice: 'Error was successfully created.' }
  #         format.json { render :show, status: :created, location: @error }
  #       else
  #         format.html { render :new }
  #         format.json { render json: @error.errors, status: :unprocessable_entity }
  #       end
  #   end
end
