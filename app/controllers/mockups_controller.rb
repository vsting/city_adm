# frozen_string_literal: true

class MockupsController < ApplicationController
  layout 'application'

  def index; end

  def news; end

  def news_one; end

  def history; end

  def symbols; end

  def gallery; end

  def gallery_one; end

  def official; end

  def message; end

  def docs; end

  def people; end

  def afisha; end

  def navigator; end

  def gov; end
end
