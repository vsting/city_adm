# frozen_string_literal: true

class Contents::AdministrationsController < ApplicationController
  def index
    @administrations = Administrations.all
  end

  private

  def administration_params
    params.require(:administration).permit(:title, :text)
  end
end
