# frozen_string_literal: true

class Contents::ForGuestsController < ApplicationController
  before_action :set_for_guest, only: %i[show edit update destroy]

  # GET /for_guests
  # GET /for_guests.json
  def index
    @for_guests = ForGuest.all
  end

  # GET /for_guests/1
  # GET /for_guests/1.json
  def show; end

  # GET /for_guests/new
  def new
    @for_guest = ForGuest.new
  end

  # GET /for_guests/1/edit
  def edit; end

  # POST /for_guests
  # POST /for_guests.json
  def create
    @for_guest = ForGuest.new(for_guest_params)

    respond_to do |format|
      if @for_guest.save
        format.html { redirect_to @for_guest, notice: 'For guest was successfully created.' }
        format.json { render :show, status: :created, location: @for_guest }
      else
        format.html { render :new }
        format.json { render json: @for_guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /for_guests/1
  # PATCH/PUT /for_guests/1.json
  def update
    respond_to do |format|
      if @for_guest.update(for_guest_params)
        format.html { redirect_to @for_guest, notice: 'For guest was successfully updated.' }
        format.json { render :show, status: :ok, location: @for_guest }
      else
        format.html { render :edit }
        format.json { render json: @for_guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /for_guests/1
  # DELETE /for_guests/1.json
  def destroy
    @for_guest.destroy
    respond_to do |format|
      format.html { redirect_to for_guests_url, notice: 'For guest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_for_guest
    @for_guest = ForGuest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def for_guest_params
    params.require(:for_guest).permit(:title, :text)
  end
end
