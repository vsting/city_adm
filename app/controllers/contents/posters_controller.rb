# frozen_string_literal: true

class Contents::PostersController < ApplicationController
  before_action :set_poster, only: %i[show edit update destroy]

  # GET /posters
  # GET /posters.json
  def index
    @posters_index ||= Poster.all
    date_grouping

    respond_to do |format|
      if_month
      if_year
      if_clear

      format.html
      format.js
      format.json
    end
  end

  # GET /posters/1
  # GET /posters/1.json
  def show
    @user = User.find(@poster.author_id)
  end

  # GET /posters/new
  def new
    @poster = Poster.new
  end

  # GET /posters/1/edit
  def edit; end

  # POST /posters
  # POST /posters.json
  def create
    @poster = Poster.new(poster_params)

    respond_to do |format|
      if @poster.save
        format.html { redirect_to @poster, notice: 'Poster was successfully created.' }
        format.json { render :show, status: :created, location: @poster }
      else
        format.html { render :new }
        format.json { render json: @poster.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posters/1
  # PATCH/PUT /posters/1.json
  def update
    respond_to do |format|
      if @poster.update(poster_params)
        format.html { redirect_to @poster, notice: 'Poster was successfully updated.' }
        format.json { render :show, status: :ok, location: @poster }
      else
        format.html { render :edit }
        format.json { render json: @poster.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posters/1
  # DELETE /posters/1.json
  def destroy
    @poster.destroy
    respond_to do |format|
      format.html { redirect_to posters_url, notice: 'Poster was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_poster
    @poster = Poster.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def poster_params
    params.require(:poster).permit(:title, :text, :description, :grey_block, :date, :time, :for_date, :image)
  end

  def if_month
    if params[:month] && !params[:year]
      month = params[:month].to_s
      month.gsub!(/[^0-9]/, '')
      @posters_index ||= Poster.where("cast(strftime('%m', created_at) as int) = ?", month)
      # @news_all = News.where(:created_at => Time.now.beginning_of_month..Time.now.end_of_month)
    end
  end

  def if_year
    if params[:year] && !params[:month]
      year = params[:year].to_s
      year.gsub!(/[^0-9]/, '')
      @posters_index ||= Poster.where("cast(strftime('%Y', created_at) as int) = ?", year)
    end
  end

  def if_clear
    @posters_index ||= Poster.all if !params[:year] && !params[:month]
  end

  def date_grouping
    posters_by_dates = Poster.order('created_at, id').limit(50)
    @posters_months = posters_by_dates.group_by { |t| t.created_at.strftime('%B') }
    @posters_years = posters_by_dates.group_by { |t| t.created_at.strftime('%Y') }
  end
end
