# frozen_string_literal: true

class Contents::SymbologiesController < ApplicationController
  before_action :set_symbology, only: %i[show edit update destroy]

  # GET /symbologies
  # GET /symbologies.json
  def index
    @symbologies = Symbology.all
  end

  # GET /symbologies/1
  # GET /symbologies/1.json
  def show; end

  # GET /symbologies/new
  def new
    @symbology = Symbology.new
  end

  # GET /symbologies/1/edit
  def edit; end

  # POST /symbologies
  # POST /symbologies.json
  def create
    @symbology = Symbology.new(symbology_params)

    respond_to do |format|
      if @symbology.save
        format.html { redirect_to @symbology, notice: 'Symbology was successfully created.' }
        format.json { render :show, status: :created, location: @symbology }
      else
        format.html { render :new }
        format.json { render json: @symbology.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /symbologies/1
  # PATCH/PUT /symbologies/1.json
  def update
    respond_to do |format|
      if @symbology.update(symbology_params)
        format.html { redirect_to @symbology, notice: 'Symbology was successfully updated.' }
        format.json { render :show, status: :ok, location: @symbology }
      else
        format.html { render :edit }
        format.json { render json: @symbology.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /symbologies/1
  # DELETE /symbologies/1.json
  def destroy
    @symbology.destroy
    respond_to do |format|
      format.html { redirect_to symbologies_url, notice: 'Symbology was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_symbology
    @symbology = Symbology.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def symbology_params
    params.require(:symbology).permit(:title, :text)
  end
end
