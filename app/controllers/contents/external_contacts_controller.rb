# frozen_string_literal: true

class Contents::ExternalContactsController < ApplicationController
  before_action :set_external_contact, only: %i[show edit update destroy]

  # GET /external_contacts
  # GET /external_contacts.json
  def index
    # @external_contacts = ExternalContact.all
    @external_contacts = ExternalContact.where("title = 'внешние связи города' OR title = 'main one'").first
  end

  # GET /external_contacts/1
  # GET /external_contacts/1.json
  def show; end

  # GET /external_contacts/new
  def new
    # @external_contact = ExternalContact.new

    @external_contact = ExternalContact.new(parent_id: params[:parent_id])
  end

  # GET /external_contacts/1/edit
  def edit; end

  # POST /external_contacts
  # POST /external_contacts.json
  def create
    @external_contact = ExternalContact.new(external_contact_params)

    respond_to do |format|
      if @external_contact.save
        format.html { redirect_to @external_contact, notice: 'External contact was successfully created.' }
        format.json { render :show, status: :created, location: @external_contact }
      else
        format.html { render :new }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /external_contacts/1
  # PATCH/PUT /external_contacts/1.json
  def update
    respond_to do |format|
      if @external_contact.update(external_contact_params)
        format.html { redirect_to @external_contact, notice: 'External contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @external_contact }
      else
        format.html { render :edit }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /external_contacts/1
  # DELETE /external_contacts/1.json
  def destroy
    @external_contact.destroy
    respond_to do |format|
      format.html { redirect_to external_contacts_url, notice: 'External contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_external_contact
    @external_contact = ExternalContact.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def external_contact_params
    params.require(:external_contact).permit(:parent_id, :title, :text, :attacheable_type, attachments_attributes: [:attach])
    # params.require(:external_contact).permit(:name, :text)
  end
end
