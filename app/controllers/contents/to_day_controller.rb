# frozen_string_literal: true

class Contents::ToDayController < ApplicationController
  def index; end

  def show; end
end
