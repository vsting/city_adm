# frozen_string_literal: true

class Contents::FeedbackController < ApplicationController
  def create
    if verify_recaptcha
      FeedbackMailer.feedback(feedback_params).deliver_later
      flash[:success] = 'Ваше сообщение отправлено'
    end
    redirect_to root_path
  end

  private

  def feedback_params
    params.permit(:name, :surname, :email, :message)
  end
end
