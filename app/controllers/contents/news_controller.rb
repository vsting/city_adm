# frozen_string_literal: true

class Contents::NewsController < ApplicationController
  before_action :set_news, only: %i[show edit update destroy]

  # GET /news
  # GET /news.json

  def index
    # @news_index ||= News.limit(20)
    date_grouping

    respond_to do |format|
      if_search
      if_month
      if_year
      if_clear

      format.html { render :index }
      format.js
      format.json
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
    # @news_all = News.all
    @user = User.find(@news_one.author_id)

    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end

  # GET /news/new
  def new
    @news = News.new
  end

  # GET /news/1/edit
  def edit; end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'News was successfully created.' }
        format.json { render :show, status: :created, location: @news }
      else
        format.html { render :new }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @news.update(news_params)
        format.html { redirect_to @news, notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: @news }
      else
        format.html { render :edit }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.destroy
    respond_to do |format|
      format.html { redirect_to news_index_url, notice: 'News was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_news
    @news_one = News.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def news_params
    params.require(:news).permit(:title, :text)
  end

  def if_search
    if params[:string] && !params[:month] && !params[:year]
      @string = params[:string].to_s
      @string = @string.sql_clean(:string)

      @news_index ||= News.where("title LIKE '%#{@string}%' OR text LIKE '%#{@string}%'").limit(50)
      true
    else
      false
    end
  end

  def if_month
    if params[:month] && !params[:string] && !params[:year]
      month = params[:month].to_s
      month = month.sql_clean(:number)
      @news_index ||= News.where("DATE_FORMAT(created_at, '%m') = #{month}")
    end
  end

  def if_year
    if params[:year] && !params[:string] && !params[:month]
      year = params[:year].to_s
      year = year.sql_clean(:number)
      @news_index ||= News.where("DATE_FORMAT(created_at, '%Y') =  #{year}")
    end
  end

  def if_clear
    if !params[:year] && !params[:string] && !params[:month]
      @news_index ||= News.all
    end
  end

  def date_grouping
    news_by_dates = News.order('created_at, id').limit(50)
    @months = news_by_dates.group_by { |t| t.created_at.strftime('%B') }
    @years = news_by_dates.group_by { |t| t.created_at.strftime('%Y') }
  end
end
