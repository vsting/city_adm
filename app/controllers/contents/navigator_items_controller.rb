# frozen_string_literal: true

class Contents::NavigatorItemsController < ApplicationController
  before_action :set_navigator_item, only: %i[show edit update destroy]

  # GET /navigator_items
  # GET /navigator_items.json
  def index
    @navigator_items = NavigatorItem.all
  end

  # GET /navigator_items/1
  # GET /navigator_items/1.json
  def show; end

  # GET /navigator_items/new
  def new
    @navigator_item = NavigatorItem.new
  end

  # GET /navigator_items/1/edit
  def edit; end

  # POST /navigator_items
  # POST /navigator_items.json
  def create
    @navigator_item = NavigatorItem.new(navigator_item_params)

    respond_to do |format|
      if @navigator_item.save
        format.html { redirect_to @navigator_item, notice: 'Navigator item was successfully created.' }
        format.json { render :show, status: :created, location: @navigator_item }
      else
        format.html { render :new }
        format.json { render json: @navigator_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /navigator_items/1
  # PATCH/PUT /navigator_items/1.json
  def update
    respond_to do |format|
      if @navigator_item.update(navigator_item_params)
        format.html { redirect_to @navigator_item, notice: 'Navigator item was successfully updated.' }
        format.json { render :show, status: :ok, location: @navigator_item }
      else
        format.html { render :edit }
        format.json { render json: @navigator_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /navigator_items/1
  # DELETE /navigator_items/1.json
  def destroy
    @navigator_item.destroy
    respond_to do |format|
      format.html { redirect_to navigator_items_url, notice: 'Navigator item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_navigator_item
    @navigator_item = NavigatorItem.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def navigator_item_params
    params.require(:navigator_item).permit(:title, :points)
  end
end
