# frozen_string_literal: true

class Contents::ForSettlersController < ApplicationController
  before_action :set_for_settler, only: %i[show edit update destroy]

  # GET /for_settlers
  # GET /for_settlers.json
  def index
    @for_settlers = ForSettler.all
  end

  # GET /for_settlers/1
  # GET /for_settlers/1.json
  def show; end

  # GET /for_settlers/new
  def new
    @for_settler = ForSettler.new
  end

  # GET /for_settlers/1/edit
  def edit; end

  # POST /for_settlers
  # POST /for_settlers.json
  def create
    @for_settler = ForSettler.new(for_settler_params)

    respond_to do |format|
      if @for_settler.save
        format.html { redirect_to @for_settler, notice: 'For settler was successfully created.' }
        format.json { render :show, status: :created, location: @for_settler }
      else
        format.html { render :new }
        format.json { render json: @for_settler.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /for_settlers/1
  # PATCH/PUT /for_settlers/1.json
  def update
    respond_to do |format|
      if @for_settler.update(for_settler_params)
        format.html { redirect_to @for_settler, notice: 'For settler was successfully updated.' }
        format.json { render :show, status: :ok, location: @for_settler }
      else
        format.html { render :edit }
        format.json { render json: @for_settler.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /for_settlers/1
  # DELETE /for_settlers/1.json
  def destroy
    @for_settler.destroy
    respond_to do |format|
      format.html { redirect_to for_settlers_url, notice: 'For settler was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_for_settler
    @for_settler = ForSettler.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def for_settler_params
    params.require(:for_settler).permit(:title, :text)
  end
end
