# frozen_string_literal: true

class Contents::AnticorruptsController < ApplicationController
  before_action :set_anticorrupt, only: %i[show edit update destroy]

  # GET /anticorrupts
  # GET /anticorrupts.json
  def index
    # @anticorrupts = Anticorrupt.all
    if_search
    if_year
    partition_grouping

    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end

  # GET /anticorrupts/1
  # GET /anticorrupts/1.json
  def show; end
  #
  #   # GET /anticorrupts/new
  #   def new
  #     @anticorrupt = Anticorrupt.new
  #   end
  #
  #   # GET /anticorrupts/1/edit
  #   def edit
  #   end
  #
  #   # POST /anticorrupts
  #   # POST /anticorrupts.json
  #   def create
  #     @anticorrupt = Anticorrupt.new(anticorrupt_params)
  #
  #     respond_to do |format|
  #       if @anticorrupt.save
  #         format.html { redirect_to @anticorrupt, notice: 'Anticorrupt was successfully created.' }
  #         format.json { render :show, status: :created, location: @anticorrupt }
  #       else
  #         format.html { render :new }
  #         format.json { render json: @anticorrupt.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # PATCH/PUT /anticorrupts/1
  #   # PATCH/PUT /anticorrupts/1.json
  #   def update
  #     respond_to do |format|
  #       if @anticorrupt.update(anticorrupt_params)
  #         format.html { redirect_to @anticorrupt, notice: 'Anticorrupt was successfully updated.' }
  #         format.json { render :show, status: :ok, location: @anticorrupt }
  #       else
  #         format.html { render :edit }
  #         format.json { render json: @anticorrupt.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # DELETE /anticorrupts/1
  #   # DELETE /anticorrupts/1.json
  #   def destroy
  #     @anticorrupt.destroy
  #     respond_to do |format|
  #       format.html { redirect_to anticorrupts_url, notice: 'Anticorrupt was successfully destroyed.' }
  #       format.json { head :no_content }
  #     end
  #   end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_anticorrupt
    @anticorrupt = Anticorrupt.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def anticorrupt_params
    params.require(:anticorrupt).permit(:name, :text)
  end

  #===============================================================================
  #===============================================================================
  #===============================================================================

  def if_search
    if params[:string] && !params[:year]
      @string = params[:string].to_s
      @string.gsub!(/[\#\.\/\{\}\\]/, '')

      @attachments = Attachment.where("attach_file_name LIKE \"%#{@string}%\"")
      @anticorrupts ||= Anticorrupt.joins(:attachments)
                                   .where(@attachments.map { "attach_file_name like '%#{@string}%'" }.join(' or '))
                                   .group('id')
    end
  end

  def if_year
    if params[:year] && !params[:string]
      @year = params[:year].to_s
      @year.gsub!(/[^0-9]/, '')
      @anticorrupts ||= Anticorrupt.where("cast(strftime('%Y', created_at) as int) = ?", @year)
    end
  end

  def partition_grouping
    @anticorrupts ||= Anticorrupt.order('partition, id').limit(50)
    @anticorrupt_partitions = @anticorrupts.group_by(&:partition)
  end
end
