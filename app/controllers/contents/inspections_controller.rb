# frozen_string_literal: true

class Contents::InspectionsController < ApplicationController
  before_action :set_inspection, only: %i[show edit update destroy]

  # GET /inspections
  # GET /inspections.json
  def index
    # @inspections = Inspection.all
    if_search
    if_year
    partition_grouping

    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end

  # GET /inspections/1
  # GET /inspections/1.json
  def show; end
  #
  #   # GET /inspections/new
  #   def new
  #     @inspection = Inspection.new
  #   end
  #
  #   # GET /inspections/1/edit
  #   def edit
  #   end
  #
  #   # POST /inspections
  #   # POST /inspections.json
  #   def create
  #     @inspection = Inspection.new(inspection_params)
  #
  #     respond_to do |format|
  #       if @inspection.save
  #         format.html { redirect_to @inspection, notice: 'Inspection was successfully created.' }
  #         format.json { render :show, status: :created, location: @inspection }
  #       else
  #         format.html { render :new }
  #         format.json { render json: @inspection.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # PATCH/PUT /inspections/1
  #   # PATCH/PUT /inspections/1.json
  #   def update
  #     respond_to do |format|
  #       if @inspection.update(inspection_params)
  #         format.html { redirect_to @inspection, notice: 'Inspection was successfully updated.' }
  #         format.json { render :show, status: :ok, location: @inspection }
  #       else
  #         format.html { render :edit }
  #         format.json { render json: @inspection.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # DELETE /inspections/1
  #   # DELETE /inspections/1.json
  #   def destroy
  #     @inspection.destroy
  #     respond_to do |format|
  #       format.html { redirect_to inspections_url, notice: 'Inspection was successfully destroyed.' }
  #       format.json { head :no_content }
  #     end
  #   end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inspection
    @inspection = Inspection.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def inspection_params
    params.require(:inspection).permit(:name, :text)
  end

  #===============================================================================
  #===============================================================================
  #===============================================================================

  def if_search
    if params[:string] && !params[:year]
      @string = params[:string].to_s
      @string.gsub!(/[\#\.\/\{\}\\]/, '')

      @attachments = Attachment.where("attach_file_name LIKE \"%#{@string}%\"")
      @inspections ||= Inspection.joins(:attachments)
                                 .where(@attachments.map { "attach_file_name like '%#{@string}%'" }.join(' or '))
                                 .group('id')
    end
  end

  def if_year
    if params[:year] && !params[:string]
      @year = params[:year].to_s
      @year.gsub!(/[^0-9]/, '')
      @inspections ||= Inspection.where("cast(strftime('%Y', created_at) as int) = ?", @year)
    end
  end

  def partition_grouping
    @inspections ||= Inspection.order('partition, id').limit(50)
    @inspection_partitions = @inspections.group_by(&:partition)
  end
end
