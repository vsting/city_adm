# frozen_string_literal: true

class Contents::DocumentsController < ApplicationController
  before_action :set_document, only: %i[show edit update destroy]

  # GET /documents
  # GET /documents.json
  def index
    # @documents = Document.all
    if_search
    if_year
    partition_grouping

    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end

  # GET /documents/1
  # GET /documents/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_document
    @document = Document.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def document_params
    params.require(:document).permit(:title, :text, :partition, :context, :name)
  end

  def if_search
    if params[:string] && !params[:year]
      @string = params[:string].to_s
      @string = @string.sql_clean(:string)

      @attachments = Attachment.where("attach_file_name LIKE '%#{@string}%'")
      @documents ||= Document.joins(:attachments)
                             .where(@attachments.map { "attach_file_name like '%#{@string}%'" }.join(' or '))
                             .group('id')
    end
  end

  def if_year
    if params[:year] && !params[:string]
      @year = params[:year].to_s
      @year = @year.sql_clean(:number)
      @documents ||= Document.where("DATE_FORMAT(created_at, '%Y') = #{@year}")
    end
  end

  def partition_grouping
    @documents ||= Document.order('partition, id').limit(50)
    @document_partitions = @documents.group_by(&:partition)
  end
end
