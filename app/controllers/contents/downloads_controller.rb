# frozen_string_literal: true

class DownloadsController < ApplicationController
  def file
    # @asset = Boxasset.find(params[:asset_id])
    # send_file Rails.root.join('private', @asset.boxasset.path), :type => @asset.boxasset_content_type, :x_sendfile => true

    @fileasset = Attachment.find(params[:id])
    send_file Rails.root.join('private', @fileasset.attach.path),
              type: @fileasset.attach_content_type, x_sendfile: true
  end
end
