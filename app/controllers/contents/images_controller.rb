# frozen_string_literal: true

class Contents::ImagesController < InheritedResources::Base
  private

    def image_params
      params.require(:image).permit(:name, :title, :text)
    end
end
