# frozen_string_literal: true

class Contents::StickersController < InheritedResources::Base
  def new
    @sticker = Sticker.new(sticker_type: params[:sticker_type])
  end

  private

    def sticker_params
      params.require(:sticker).permit(:title, :text, :color, :stickerable_id, :stickerable_type)
    end
end
