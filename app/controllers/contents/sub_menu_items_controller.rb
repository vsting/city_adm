# frozen_string_literal: true

class Contents::SubMenuItemsController < ApplicationController
  before_action :set_sub_menu_item, only: %i[show edit update destroy]

  # GET /sub_menu_items
  # GET /sub_menu_items.json
  def index
    @sub_menu_items = SubMenuItem.all
  end

  # GET /sub_menu_items/1
  # GET /sub_menu_items/1.json
  def show; end

  # GET /sub_menu_items/new
  def new
    @sub_menu_item = SubMenuItem.new
  end

  # GET /sub_menu_items/1/edit
  def edit; end

  # POST /sub_menu_items
  # POST /sub_menu_items.json
  def create
    @sub_menu_item = SubMenuItem.new(sub_menu_item_params)

    respond_to do |format|
      if @sub_menu_item.save
        format.html { redirect_to @sub_menu_item, notice: 'Sub menu item was successfully created.' }
        format.json { render :show, status: :created, location: @sub_menu_item }
      else
        format.html { render :new }
        format.json { render json: @sub_menu_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_menu_items/1
  # PATCH/PUT /sub_menu_items/1.json
  def update
    respond_to do |format|
      if @sub_menu_item.update(sub_menu_item_params)
        format.html { redirect_to @sub_menu_item, notice: 'Sub menu item was successfully updated.' }
        format.json { render :show, status: :ok, location: @sub_menu_item }
      else
        format.html { render :edit }
        format.json { render json: @sub_menu_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_menu_items/1
  # DELETE /sub_menu_items/1.json
  def destroy
    @sub_menu_item.destroy
    respond_to do |format|
      format.html { redirect_to sub_menu_items_url, notice: 'Sub menu item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_sub_menu_item
    @sub_menu_item = SubMenuItem.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sub_menu_item_params
    params.require(:sub_menu_item).permit(:menu_item_id, :name, :title)
  end
end
