# frozen_string_literal: true

class Contents::InspectionRatingsController < ApplicationController
  before_action :set_inspection_rating, only: %i[show edit update destroy]

  # GET /inspection_ratings
  # GET /inspection_ratings.json
  def index
    # @inspection_ratings = InspectionRating.all
    if_search
    if_year
    partition_grouping

    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end

  # GET /inspection_ratings/1
  # GET /inspection_ratings/1.json
  def show; end
  #
  #   # GET /inspection_ratings/new
  #   def new
  #     @inspection_rating = InspectionRating.new
  #   end
  #
  #   # GET /inspection_ratings/1/edit
  #   def edit
  #   end
  #
  #   # POST /inspection_ratings
  #   # POST /inspection_ratings.json
  #   def create
  #     @inspection_rating = InspectionRating.new(inspection_rating_params)
  #
  #     respond_to do |format|
  #       if @inspection_rating.save
  #         format.html { redirect_to @inspection_rating, notice: 'Inspection rating was successfully created.' }
  #         format.json { render :show, status: :created, location: @inspection_rating }
  #       else
  #         format.html { render :new }
  #         format.json { render json: @inspection_rating.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # PATCH/PUT /inspection_ratings/1
  #   # PATCH/PUT /inspection_ratings/1.json
  #   def update
  #     respond_to do |format|
  #       if @inspection_rating.update(inspection_rating_params)
  #         format.html { redirect_to @inspection_rating, notice: 'Inspection rating was successfully updated.' }
  #         format.json { render :show, status: :ok, location: @inspection_rating }
  #       else
  #         format.html { render :edit }
  #         format.json { render json: @inspection_rating.errors, status: :unprocessable_entity }
  #       end
  #     end
  #   end
  #
  #   # DELETE /inspection_ratings/1
  #   # DELETE /inspection_ratings/1.json
  #   def destroy
  #     @inspection_rating.destroy
  #     respond_to do |format|
  #       format.html { redirect_to inspection_ratings_url, notice: 'Inspection rating was successfully destroyed.' }
  #       format.json { head :no_content }
  #     end
  #   end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inspection_rating
    @inspection_rating = InspectionRating.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def inspection_rating_params
    params.require(:inspection_rating).permit(:title, :text)
  end

  #===============================================================================
  #===============================================================================
  #===============================================================================

  def if_search
    if params[:string] && !params[:year]
      @string = params[:string].to_s
      @string.gsub!(/[\#\.\/\{\}\\]/, '')

      @attachments = Attachment.where("attach_file_name LIKE \"%#{@string}%\"")
      @inspection_ratings ||= InspectionRating.joins(:attachments)
                                              .where(@attachments.map { "attach_file_name like '%#{@string}%'" }.join(' or '))
                                              .group('id')
    end
  end

  def if_year
    if params[:year] && !params[:string]
      @year = params[:year].to_s
      @year.gsub!(/[^0-9]/, '')
      @inspection_ratings ||= InspectionRating.where("cast(strftime('%Y', created_at) as int) = ?", @year)
    end
  end

  def partition_grouping
    @inspection_ratings ||= InspectionRating.order('partition, id').limit(50)
    @inspection_ratings_partitions = @inspection_ratings.group_by(&:partition)
  end
end
