# frozen_string_literal: true

class Contents::ExternalContactContentsController < InheritedResources::Base
  private

    def external_contact_content_params
      params.require(:external_contact_content).permit(:title, :text)
    end
end
