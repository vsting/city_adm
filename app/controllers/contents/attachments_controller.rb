# frozen_string_literal: true

class Contents::AttachmentsController < InheritedResources::Base
  private

    def attachment_params
      params.require(:attachment).permit(:name, :title, :text, :attacheable_id, :attacheable_type)
    end
end
