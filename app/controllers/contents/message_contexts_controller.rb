# frozen_string_literal: true

class Contents::MessageContextsController < ApplicationController
  before_action :set_message_context, only: %i[show edit update destroy]

  # GET /message_contexts
  # GET /message_contexts.json
  def index
    @message_contexts = MessageContext.all
  end

  # GET /message_contexts/1
  # GET /message_contexts/1.json
  def show; end

  # GET /message_contexts/new
  def new
    @message_context = MessageContext.new
  end

  # GET /message_contexts/1/edit
  def edit; end

  # POST /message_contexts
  # POST /message_contexts.json
  def create
    @message_context = MessageContext.new(message_context_params)

    respond_to do |format|
      if @message_context.save
        format.html { redirect_to @message_context, notice: 'Message context was successfully created.' }
        format.json { render :show, status: :created, location: @message_context }
      else
        format.html { render :new }
        format.json { render json: @message_context.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /message_contexts/1
  # PATCH/PUT /message_contexts/1.json
  def update
    respond_to do |format|
      if @message_context.update(message_context_params)
        format.html { redirect_to @message_context, notice: 'Message context was successfully updated.' }
        format.json { render :show, status: :ok, location: @message_context }
      else
        format.html { render :edit }
        format.json { render json: @message_context.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /message_contexts/1
  # DELETE /message_contexts/1.json
  def destroy
    @message_context.destroy
    respond_to do |format|
      format.html { redirect_to message_contexts_url, notice: 'Message context was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_message_context
    @message_context = MessageContext.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def message_context_params
    params.require(:message_context).permit(:title, :text)
  end
end
