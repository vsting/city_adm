# frozen_string_literal: true

class SynodsController < InheritedResources::Base
  private

    def synod_params
      params.require(:synod).permit(:title, :text)
    end
end
