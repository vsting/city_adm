# frozen_string_literal: true

class MainController < ApplicationController
  def index; end

  def all; end

  def today; end
end
