# frozen_string_literal: true

module ApplicationHelper
  # supply namespace, i.e. :desktop
  # filename, i.e 'app' - for app.css
  # method, default is :relative
  # change it to :filesystem for filesystem path of :external for absolute external path
  # in case of external path, you should also supply :hostname parameter
  def custom_stylesheet_tag(options)
    filename = FrontendBuildFinder.new(:css, options).perform
    tag(:link, 'rel' => 'stylesheet', 'href' => filename)
  end

  def custom_javascript_tag(options)
    filename = FrontendBuildFinder.new(:js, options).perform
    content_tag(:script, 'src' => filename) {}
  end

  def title(page_name = nil)
    " / #{page_name} / #{action_name}" unless page_name.nil? && action_name.nil?
    " / #{action_name}" if page_name.nil? && !action_name.nil?
    '' if page_name.nil? && action_name.nil?
  end
end
