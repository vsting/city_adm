module ActiveAdmin::StickersHelper
  def parent_link(parent_instance)
    [:admin, url_for(parent_instance).gsub!("/",'')]
  end
  #
  #   def nested_sub(mainmenus)
  #     mainmenus.map do |mm, sm|
  #       @childs = Mainmenu.where(:mid => mm.id, :menutype => "child")
  #
  #       if mm.menutype != "link"
  #         @cls = "dropdown" if mm.menutype == "main"
  #         @cls = "dropdown-submenu" if mm.menutype != "main"
  #         @cls = "dropdown with-childmenu" if mm.menutype == "main" && (@childs.size > 0 || sm.size > 0)
  #       else
  #         @cls = "link" if mm.menutype == "link"
  #       end
  #
  #       render partial: "mainmenus/topmenu/menu.haml", locals: {:mm => mm, :sm => sm}
  #     end.join.html_safe
  #   end
  #
  #   def admin_nested_mainmenus(mm)
  #     render "mainmenus/admin/menu", :mainmenus => mm
  #   end
end