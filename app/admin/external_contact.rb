# frozen_string_literal: true

ActiveAdmin.register ExternalContact do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :title, :text, :ancestry, :parent, :parent_id, :children
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', label: 'Внешние связи города'
  form partial: 'form'

  controller do
    def new
      @external_contact = ExternalContact.new
      @external_contact.ancestry = params[:ancestry]
      @external_contact.parent_id = params[:ancestry]
    end

    def edit
      @external_contact = ExternalContact.find(params[:id])
    end
  end

  member_action :create, method: :post do
    @external_contact = ExternalContact.new(params.require(:external_contact).permit(:title, :text, :ancestry, :parent_id, attachments_attributes: [:attach]))

    respond_to do |format|
      if @external_contact.save
        format.html { redirect_to [:admin, @external_contact], notice: 'External Contact was successfully created.' }
        format.json { render :show, status: :created, location: @external_contact }
      else
        format.html { render :new }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @external_contact = ExternalContact.find(params[:id])

    respond_to do |format|
      if @external_contact.update(params.require(:external_contact).permit(:title, :text, :ancestry, :parent_id, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @external_contact], notice: 'External Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @external_contact] }
      else
        format.html { render :edit }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @external_contact = ExternalContact.find(params[:id])

    unless @external_contact.parent.nil?
      div do
        panel 'Родительские раздел' do
          render 'admin/external_contacts/parent'
        end
      end
    end

    attributes_table do
      row :parent_id
      row :title
      row :text
    end

    div do
      panel 'файлы' do
        div class: 'admin-attaches-list' do
          @external_contact.attachments.each do |attache|
            div class: 'admin_attache_block', id: "file-block-thumb_#{attache.id}" do
              div class: 'admin_attaches_list' do
                div class: 'file-line' do
                  link_to attache.short_title, attache.attach.url(:original)
                end
              end
              render 'admin/external_contacts/attach_functions', attache: attache
            end
          end
        end
      end
    end

    div do
      unless @external_contact.children.empty?
        panel 'подразделы' do
          render 'admin/external_contacts/sub'
        end
      end

      render 'admin/external_contacts/sub_functions'
    end
  end

  index do
    selectable_column
    column 'Title' do |ec|
      link_to ec.title, admin_external_contact_path(ec)
    end
    actions
  end
end
