# frozen_string_literal: true

ActiveAdmin.register Anticorrupt do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :title, :text, :name, :partition, :year
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', label: 'Антикоррупция'
  form partial: 'form'

  member_action :create, method: :post do
    # prm2 = params.require(:sticker).permit(:title, :text, :color)
    @anticorrupt = Anticorrupt.new(params.require(:anticorrupt).permit(:title, :text, :partition, :year, :name, attachments_attributes: [:attach]))

    respond_to do |format|
      # if @anticorrupt.save and @anticorrupt.stickers.create!(title: prm[:sticker][:title], text: prm[:sticker][:text], color: prm[:sticker][:color])
      if @anticorrupt.save
        format.html { redirect_to [:admin, @anticorrupt], notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @anticorrupt }
      else
        format.html { render :new }
        format.json { render json: @anticorrupt.errors, status: :unprocessable_entity }
      end
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    prm = params.require(:anticorrupt).permit(:title, :text, :partition, :year, :name, attachments_attributes: [:attach])
    @anticorrupt = Anticorrupt.find(params[:id])

    respond_to do |format|
      if @anticorrupt.update(prm)
        format.html { redirect_to [:admin, @anticorrupt], notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @anticorrupt] }
      else
        format.html { render :edit }
        format.json { render json: @anticorrupt.errors, status: :unprocessable_entity }
      end
    end
  end

  index do
    id_column
    column :parent
    column :title
    column :text
    actions

    panel "стикер" do
      @sticker = Sticker.where(sticker_type: controller_name.classify).first
      if @sticker.nil?
        render 'admin/stickers/new', class_name: controller_name.classify
      else
        render 'admin/stickers/sticker', sticker: @sticker
      end
    end
  end

  show do
    @model = Anticorrupt.find(params[:id])
    @anticorrupt = Anticorrupt.find(params[:id])

    attributes_table do
      row :name
      row :year
      row :title
      row :partition
      row :text
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        # render "admin/attachment/list"
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attache_block', id: "file-block-thumb_#{attache.id}" do
              div class: 'admin_attaches_list' do
                div class: 'file-line' do
                  link_to attache.short_title, attache.attach.url(:original)
                end
              end

              render 'admin/anticorrupts/functions', attache: attache
            end
          end
        end
      end
    end
  end
end
