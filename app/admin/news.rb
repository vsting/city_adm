# frozen_string_literal: true

ActiveAdmin.register News do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :author_id, :description, :grey_block, :title, :text, :tag, :attachments
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Общее', label: 'Новости'
  form partial: 'form'

  #   form :html => { :enctype => "multipart/form-data" } do |f|
  #     f.inputs do
  #       f.input :title, label: "заголовок", placeholder: 'заголовок', required: true
  #       f.input :description, label: "описание", placeholder: 'краткое описание', input_html: {style: "height: 100px;"}
  #       f.input :grey_block, label: "текстовый блок", placeholder: 'текстовая блочная вставка', input_html: {style: "height: 100px;"}
  #       f.input :text, label: "основной текст", placeholder: 'основной текст', required: true, input_html: {style: "height: 100px;"}
  #
  #       #f.fields_for :attachments, news.attachments.build do |a|
  #       f.fields_for :attachments, news.attachments.new, :html => { :enctype => "multipart/form-data" } do |a|
  #         #if a.object.new_record?
  #         a.input :attach, as: :file, multiple: true
  #         a.input :attacheable_type, input_html: { value: news.class.name }, as: :hidden
  #         #end
  #       end
  #
  #     end
  #     f.actions
  #   end

  member_action :create, method: :post do
    @news = News.new(params.require(:news).permit(:author_id, :title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))

    if @news.save
      flash[:notice] = 'Successfully created News.'
      redirect_to [:admin, @news]
    else
      render :new
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @news = News.find(params[:id])

    respond_to do |format|
      if @news.update(params.require(:news).permit(:author_id, :title, :text, :description, :grey_block, :time, :date, :for_date, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @news], notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @news] }
      else
        format.html { render :edit }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  index do
    selectable_column
    id_column
    column :author_id
    column :title
    column :text
    actions
  end

  show do
    @model = News.find(params[:id])

    attributes_table do
      row :author_id
      row :title
      row :text
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attaches_thumb_list', id: "file-block-thumb_#{attache.id}" do
              div class: 'file-block-thumb' do
                image_tag attache.attach.url(:thumb)
              end
              div do
                link_to 'открыть', admin_attachment_path(attache)
                ' | '
                link_to 'удалить', admin_attachment_path(attache), method: :delete, remote: true
              end
            end
          end
        end
      end
    end
  end
end
