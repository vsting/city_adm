# frozen_string_literal: true

ActiveAdmin.register Vacancy do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', label: 'Вакансии'

  member_action :create, method: :post do
    @vacancy = Vacancy.new(params.require(:vacancy).permit(:title, :text, :attacheable_type, attachments_attributes: [:attach]))

    respond_to do |format|
      if @vacancy.save
        format.html { redirect_to [:admin, @vacancy], notice: 'Vacancy was successfully created.' }
        format.json { render :show, status: :created, location: @vacancy }
      else
        format.html { render :new }
        format.json { render json: @vacancy.errors, status: :unprocessable_entity }
      end
    end
  end
end
