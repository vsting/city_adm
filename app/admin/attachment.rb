# frozen_string_literal: true

ActiveAdmin.register Attachment do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :name, :title, :text, :tag, :attach, :attacheable_id, :attacheable_type #:attach_file_name, :attach_content_type, :attach_file_size, :attach_updated_at
  menu label: 'Файлы'
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  #
  #   member_action :create, method: :post do
  #     owner = params.require(:attachment).permit(:owner)
  #     respond_to do |format|
  #       unless owner.nil?
  #         params[:attachment][:attach].each do |image|
  #           @attache = Attachment.create()
  #           @attache.attach = image
  #           @attache.save
  #         end
  #       else
  #         @attache = Attachment.new()
  #
  #       end
  #
  #       unless @attache.attacheable_type.nil?
  #         instance = Object.const_get(@attache.attacheable_type).find(@attache.attacheable_id)
  #         format.html { redirect_to [:admin, instance] }
  #       else
  #         format.html { redirect_to [:admin, :attachments] }
  #       end
  #
  # =begin
  #
  #       #format.html { redirect_to admin_attachment_path(@attache) }
  #         format.html { render json: { attachments: [@attache] }, :content_type => 'text/html', :layout => false }
  #         format.json { render json: { attachments: [@attache] }, status: :created, location: [:admin, @attache] }
  #
  #       else
  #         format.html { render action: "new" }
  #         format.json { render json: @attache.errors, status: :unprocessable_entity }
  #       end
  #
  #     end
  #   end

  member_action :destroy, method: :post do
    @attachment = Attachment.find(params[:id])

    respond_to do |format|
      format.js { render 'admin/attachment/destroy' } if @attachment.destroy
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs 'Attachment Details' do
      f.input :name
      f.input :title
      f.input :text
      f.input :tag
      # f.select_tag(:attachable_id, options: {option: '1', option: '2'})
      # f.input :attachable_id, :label => 'Member', :as => :select, :collection => User.all.map{|u| ["#{u.last_name}, #{u.first_name}", u.id]}
      f.input :attach, as: :file # , :hint => ad.attach_content_type =~ %r(image) ? f.template.image_tag(f.object.attach.url(:thumb)) : f.template.link_to(f.object.attach.attach_file_name, f.object.attach.url())
    end
    f.actions
  end

  index do
    column :attachments do |attachment|
      image_tag attachment.attach.url(:thumb)
    end
    column :titile, &:title
    actions
  end
  #   end
  #
  #   index do
  #     column :attachments do |attachment|
  #       attachment.attach.url
  #     end
  #     column :titile do |t|
  #       t.title
  #     end
  #     actions
  #   end

  show do
    @params = params[:action]
    attributes_table do
      row :action do
        @params
      end
      row :title
      row :attacheable_id
      row :attacheable_type
      row :attachments do |attachment|
        if attachment.attach.content_type =~ /image/
          image_tag(attachment.attach.url(:thumb))
        else
          link_to attachment.title, attachment.attach.url
        end
      end
    end
    # active_admin_comments
  end
  #
  #   show do |ad|
  #     attributes_table do
  #       row :name
  #       row :attach do
  #         if ad.attach.content_type =~ %r(image)
  #           image_tag(ad.attach.url(:thumb))
  #         else
  #           link_to(ad.attach.name, ad.attach.url(:original))
  #           #ad.attach.path
  #         end
  #       end
  #       # Will display the image on show object page
  #     end
  #   end
end
