# frozen_string_literal: true

ActiveAdmin.register Person do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  permit_params :tag, :title, :text, :name, :attachments
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', label: 'Почетные граждане'
  form partial: 'form'

  member_action :create, method: :post do
    @person = Person.new(params.require(:person).permit(:title, :text, :tag, attachments_attributes: %i[attacheable_type attach]))

    if @person.save
      flash[:notice] = 'Successfully created Person.'
      redirect_to [:admin, @person]
    else
      render :new
    end
  end

  member_action :update, method: :put do
    @person = Person.find(params[:id])

    respond_to do |format|
      if @person.update(params.require(:person).permit(:title, :text, :tag, attachments_attributes: %i[attacheable_type attach]))
        format.html { redirect_to [:admin, @person], notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @person] }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @model = Person.find(params[:id])
    attributes_table do
      row :title
      row :text
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        # render "admin/attachment/list"
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attaches_thumb_list', id: "file-block-thumb_#{attache.id}" do
              div class: 'file-block-thumb' do
                image_tag attache.attach.url(:thumb)
              end
              div do
                link_to 'открыть', admin_attachment_path(attache)
                ' | '
                link_to 'удалить', admin_attachment_path(attache), method: :delete, remote: true
              end
            end
          end
        end
      end
    end
  end
end
