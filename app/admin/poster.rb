# frozen_string_literal: true

ActiveAdmin.register Poster do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #

  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  #
  #   form :html => { :enctype => "multipart/form-data" } do |f|
  #     f.inputs do
  #       f.input :title, label: "заголовок", placeholder: 'заголовок', required: true
  #       f.input :for_date, label: "отображать до", as: :datepicker, placeholder: 'показывать до, в формате гггг-мм-дд ', required: true
  #       f.input :date, label: "дата начала", placeholder: 'дата события', required: true
  #       f.input :time, label: "время начала", placeholder: 'время события', required: true
  #       f.input :description, label: "описание", placeholder: 'краткое описание', input_html: {style: "height: 100px;"}
  #       f.input :grey_block, label: "текстовый блок", placeholder: 'текстовая блочная вставка', input_html: {style: "height: 100px;"}
  #       f.input :text, label: "основной текст", placeholder: 'основной текст', required: true, input_html: {style: "height: 100px;"}
  #
  #       f.fields_for :attachments, poster.attachments.build do |a|
  #         #if a.object.new_record?
  #           a.input :attach, as: :file, multiple: true
  #           a.input :attacheable_type, input_html: { value: poster.class.name }, as: :hidden
  #         #end
  #       end
  #
  #     end
  #     f.actions
  #   end

  permit_params :author_id, :title, :text, :attachments

  menu parent: 'Общее', label: 'Афиша'
  form partial: 'form'

  member_action :create, method: :post do
    @poster = Poster.new(params.require(:poster).permit(:author_id, :title, :text, :description, :grey_block, :time, :date, :for_date, attachments_attributes: [:attach]))

    if @poster.save
      flash[:notice] = 'Successfully created Poster.'
      redirect_to [:admin, @poster]
    else
      render :new
    end
  end

  member_action :update, method: :put do
    # @poster = Poster.new(params.require(:poster).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @poster = Poster.find(params[:id])

    respond_to do |format|
      if @poster.update(params.require(:poster).permit(:author_id, :title, :text, :description, :grey_block, :time, :date, :for_date, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @poster], notice: 'Poster was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @poster] }
      else
        format.html { render :edit }
        format.json { render json: @poster.errors, status: :unprocessable_entity }
      end
    end
  end

  index do
    selectable_column
    id_column
    column :author_id
    column :title
    column :text
    actions
  end

  show do
    @poster = Poster.find(params[:id])
    attributes_table do
      row :title
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        # render "admin/attachment/list"
        div class: 'admin-attaches-list' do
          @poster.attachments.each do |attache|
            div class: 'admin_attaches_thumb_list', id: "file-block-thumb_#{attache.id}" do
              div class: 'file-block-thumb' do
                image_tag attache.attach.url(:thumb)
              end
              div do
                link_to 'открыть', admin_attachment_path(attache)
                ' | '
                link_to 'удалить', admin_attachment_path(attache), method: :delete, remote: true
              end
            end
          end
        end
      end
    end
  end
end
