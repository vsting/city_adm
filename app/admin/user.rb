# frozen_string_literal: true

ActiveAdmin.register User do
  permit_params :name, :email, :password, :password_confirmation, :attachments

  index do
    selectable_column
    id_column
    column :name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :name
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu label: 'Пользователи'

  form partial: 'form'

  member_action :create, method: :post do
    @user = User.new(params.require(:user).permit(:name, :email, :password, :password_confirmation, attachments_attributes: %i[attacheable_type attach]))

    if @user.save
      flash[:notice] = 'Successfully created User.'
      redirect_to [:admin, @user]
    else
      render :new
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @user = User.find(params[:id])

    respond_to do |format|
      # if @user.update(params.require(:user).permit(:email, :password, :password_confirmation, attachments_attributes: [:attacheable_type, :attach]))
      if @user.update(params.require(:user).permit(:name, :email, attachments_attributes: %i[attacheable_type attach]))
        format.html { redirect_to [:admin, @user], notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @user] }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @model = User.find(params[:id])
    attributes_table do
      row :id
      row :name
      row :email
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        # render "admin/attachment/list"
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attaches_thumb_list', id: "file-block-thumb_#{attache.id}" do
              div class: 'file-block-thumb' do
                image_tag attache.attach.url(:thumb)
              end
              div do
                link_to 'открыть', admin_attachment_path(attache)
                ' | '
                link_to 'удалить', admin_attachment_path(attache), method: :delete, remote: true
              end
            end
          end
        end
      end
    end
  end
end
