# frozen_string_literal: true

ActiveAdmin.register ExternalContactContent do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :title, :text, :external_contact

  menu parent: 'Официально', label: 'Контент (Внешний связи города)'
  form partial: 'form'

  controller do
    def new
      @external_contact_content = ExternalContactContent.new(params[:external_contact_id])
    end

    def edit
      @external_contact_content = ExternalContactContent.find(params[:id])
    end
  end

  member_action :create, method: :post do
    respond_to do |format|
      @external_contact_content = ExternalContactContent.new(params.require(:external_contact_content).permit(:title, :text, :external_contact_id, :attacheable_type, attachments_attributes: [:attach]))
      if @external_contact_content.save
        format.html { redirect_to [:admin, @external_contact_content], notice: 'External Contact was successfully created.' }
        format.json { render :show, status: :created, location: @external_contact_content }
      else
        format.html { render :new }
        format.json { render json: @external_contact_content.errors, status: :unprocessable_entity }
      end
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @external_contact_content = ExternalContactContent.find(params[:id])

    respond_to do |format|
      if @external_contact_content.update(params.require(:external_contact_content).permit(:title, :text, :external_contact_id, :attacheable_type, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @external_contact_content], notice: 'External Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @external_contact_content] }
      else
        format.html { render :edit }
        format.json { render json: @external_contact_content.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @external_contact_content = ExternalContactContent.find(params[:id])

    unless @external_contact_content.parent.nil?
      div do
        panel 'Родительские раздел' do
          render 'admin/external_contact_contents/parent'
        end
      end
    end

    attributes_table do
      row :title
      row :text
    end

    div do
      panel 'файлы' do
        div class: 'admin-attaches-list' do
          @external_contact_content.attachments.each do |attache|
            div class: 'admin_attache_block', id: "file-block-thumb_#{attache.id}" do
              div class: 'admin_attaches_list' do
                div class: 'file-line' do
                  link_to attache.short_title, attache.attach.url(:original)
                end
              end
              render 'admin/external_contact_contents/functions', attache: attache
            end
          end
        end
      end
    end
  end

  index do
    selectable_column
    column 'Title' do |ec|
      link_to ec.title, admin_external_contact_content_path(ec)
    end
    actions
  end
end
