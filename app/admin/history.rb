# frozen_string_literal: true

ActiveAdmin.register History do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  permit_params :tag, :title, :text, :name, :attachments
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Общее', label: 'История города'
  form partial: 'form'

  member_action :create, method: :post do
    @history = History.new(params.require(:history).permit(:title, :text, :tag, :attacheable_type, attachments_attributes: %i[attacheable_type attach]))

    if @history.save
      flash[:notice] = 'Successfully created History.'
      redirect_to [:admin, @history]
    else
      render :new
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @history = History.find(params[:id])

    respond_to do |format|
      if @history.update(params.require(:history).permit(:title, :text, :tag, attachments_attributes: %i[attacheable_type attach]))
        format.html { redirect_to [:admin, @history], notice: 'History was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @history] }
      else
        format.html { render :edit }
        format.json { render json: @history.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @model = History.find(params[:id])
    attributes_table do
      row :title
      row :text
    end

    div do
      panel 'файлы' do
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attaches_thumb_list', id: "file-block-thumb_#{attache.id}" do
              div class: 'file-block-thumb' do
                image_tag attache.attach.url(:thumb)
              end
              div do
                link_to 'открыть', admin_attachment_path(attache)
                ' | '
                link_to 'удалить', admin_attachment_path(attache), method: :delete, remote: true
              end
            end
          end
        end
      end
    end

  end
end
