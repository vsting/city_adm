# frozen_string_literal: true

ActiveAdmin.register InspectionRating do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  permit_params :title, :text, :name, :partition, :year
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', label: 'Оценка воздействия проверяющих организаций'
  form partial: 'forms'

  member_action :create, method: :post do
    @inspection_rating = InspectionRating.new(params.require(:inspection_rating).permit(:title, :text, :partition, :year, :name, :attacheable_type, attachments_attributes: [:attach]))

    respond_to do |format|
      if @inspection_rating.save
        format.html { redirect_to [:admin, @inspection_rating], notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @inspection_rating }
      else
        format.html { render :new }
        format.json { render json: @inspection_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @inspection_rating = InspectionRating.find(params[:id])

    respond_to do |format|
      if @inspection_rating.update(params.require(:inspection_rating).permit(:title, :text, :partition, :year, :name, :attacheable_type, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @inspection_rating], notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @inspection_rating] }
      else
        format.html { render :edit }
        format.json { render json: @inspection_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @model = InspectionRating.find(params[:id])
    attributes_table do
      row :name
      row :year
      row :title
      row :partition
      row :text
    end

    div do
      panel 'файлы' do
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attache_block', id: "file-block-thumb_#{attache.id}" do
              div class: 'admin_attaches_list' do
                div class: 'file-line' do
                  link_to attache.short_title, attache.attach.url(:original)
                end
              end
              render 'admin/inspection_ratings/functions', attache: attache
            end
          end
        end
      end
    end
  end
end
