# frozen_string_literal: true

ActiveAdmin.register Document do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :mode
  permit_params :title, :text, :partition, :context, :name, :attachments, :attacheable_type
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  menu parent: 'Официально', priority: 1, label: 'Документы'
  form partial: 'forms'

  controller do
    def new
      @document = Document.new
    end

    def edit
      @document = Document.find(params[:id])
    end
  end

  member_action :create, method: :post do
    prm = params.require(:document).permit(:title, :text, :partition, :context, :name, attachments_attributes: [:attach])
    @document = Document.new(prm)

    respond_to do |format|
      if @document.save
        format.html { redirect_to [:admin, @document], notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  member_action :update, method: :put do
    # @news = News.new(params.require(:news).permit(:title, :text, :description, :grey_block, :time, :date, :for_date, :attacheable_type, attachments_attributes: [:attach]))
    @document = Document.find(params[:id])

    respond_to do |format|
      if @document.update(params.require(:document).permit(:title, :text, :partition, :context, :name, attachments_attributes: [:attach]))
        format.html { redirect_to [:admin, @document], notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @document] }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  show do
    @model = Document.find(params[:id])
    attributes_table do
      row :title
      row :text
      row :partition
      row :context
    end

    div do
      panel 'файлы' do
        # table_for news.attachments do
        #  column :title
        #  column :tag
        # end
        # render "admin/attachment/list"
        div class: 'admin-attaches-list' do
          @model.attachments.each do |attache|
            div class: 'admin_attache_block', id: "file-block-thumb_#{attache.id}" do
              div class: 'admin_attaches_list' do
                div class: 'file-line' do
                  link_to attache.short_title, attache.attach.url(:original)
                end
              end

              render 'admin/documents/functions', attache: attache
            end
          end
        end
      end
    end
  end
end
