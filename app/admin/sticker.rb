# frozen_string_literal: true

ActiveAdmin.register Sticker do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :title, :text, :color, :sticker_type
  menu parent: 'Официально', label: 'Стикеры'
  # form partial: 'forms'
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  # menu parent: 'Официально'

  form partial: 'form'

  controller do
    def new
      @sticker = Sticker.new(sticker_type: params[:sticker_type])
    end
  end

  member_action :create, method: :post do
    @sticker = Sticker.new(params.require(:sticker).permit(:title, :text, :color, :sticker_type))

    respond_to do |format|
      if @sticker.save
        format.html { redirect_to [:admin, @sticker], notice: 'Sticker was successfully created.' }
        format.json { render :show, status: :created, location: @sticker }
      else
        format.html { render :new, notice: "Такой стикер уже существует" }
        format.json { render json: @sticker.errors, status: :unprocessable_entity }
      end
    end
  end

  #   member_action :update, method: :put do
  #     #@news = News.new(params.require(:news).permit(:title, :text, :sticker_type, attachments_attributes: [:attach]))
  #     @sticker = Sticker.find(params[:id])
  #
  #     respond_to do |format|
  #       #if @sticker.update(params.require(:sticker).permit(:title, :text, :stickerable_id, :stickerable_type, :attacheable_type, attachments_attributes: [:attach]))
  #       if @sticker.update(params.require(:sticker).permit(:title, :text, :stickerable_id, :stickerable_type))
  #         format.html { redirect_to [:admin, @sticker], notice: 'Sticker was successfully updated.' }
  #         format.json { render :show, status: :ok, location: [:admin, @sticker] }
  #       else
  #         format.html { render :edit }
  #         format.json { render json: @sticker.errors, status: :unprocessable_entity }
  #       end
  #     end
  #
  #   end

  show do
    @model = Sticker.find(params[:id])
    attributes_table do
      row :sticker_type do
        link_to @model.sticker_type, parent_link(@model.parent_instance)
      end
      row :title
      row :text
      row :color
    end

    #     div do
    #       panel "файлы" do
    #         #table_for news.attachments do
    #         #  column :title
    #         #  column :tag
    #         #end
    #         #render "admin/attachment/list"
    #         div class: "admin-attaches-list" do
    #           @model.attachments.each do |attache|
    #
    #             div class: "admin_attache_block", id: "file-block-thumb_#{attache.id}" do
    #               div class: "admin_attaches_list" do
    #                 div class: "file-line" do
    #                   link_to attache.short_title, attache.attach.url(:original)
    #                 end
    #               end
    #
    #               render "admin/stickers/functions", attache: attache
    #
    #             end
    #
    #           end
    #         end
    #       end
    #     end
  end
end
