# frozen_string_literal: true

json.partial! 'statistics/statistic', statistic: @statistic
