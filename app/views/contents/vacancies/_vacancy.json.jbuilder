# frozen_string_literal: true

json.extract! vacancy, :id, :title, :text, :type, :created_at, :updated_at
json.url vacancy_url(vacancy, format: :json)
