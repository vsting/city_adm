# frozen_string_literal: true

json.partial! 'vacancies/vacancy', vacancy: @vacancy
