# frozen_string_literal: true

json.array! @vacancies, partial: 'vacancies/vacancy', as: :vacancy
