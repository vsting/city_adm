# frozen_string_literal: true

json.extract! official, :id, :title, :text, :created_at, :updated_at
json.url official_url(official, format: :json)
