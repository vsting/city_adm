# frozen_string_literal: true

json.array! @officials, partial: 'officials/official', as: :official
