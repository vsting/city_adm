# frozen_string_literal: true

json.extract! external_contact_content, :id, :title, :text, :created_at, :updated_at
json.url external_contact_content_url(external_contact_content, format: :json)
