# frozen_string_literal: true

json.partial! 'external_contact_contents/external_contact_content', external_contact_content: @external_contact_content
