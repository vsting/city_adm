# frozen_string_literal: true

json.array! @external_contact_contents, partial: 'external_contact_contents/external_contact_content', as: :external_contact_content
