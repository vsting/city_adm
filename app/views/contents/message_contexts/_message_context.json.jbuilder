# frozen_string_literal: true

json.extract! message_context, :id, :title, :text, :created_at, :updated_at
json.url message_context_url(message_context, format: :json)
