# frozen_string_literal: true

json.array! @message_contexts, partial: 'message_contexts/message_context', as: :message_context
