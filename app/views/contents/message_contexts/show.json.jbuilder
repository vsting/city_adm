# frozen_string_literal: true

json.partial! 'message_contexts/message_context', message_context: @message_context
