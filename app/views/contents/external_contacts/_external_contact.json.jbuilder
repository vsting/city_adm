# frozen_string_literal: true

json.extract! external_contact, :id, :name, :text, :created_at, :updated_at
json.url external_contact_url(external_contact, format: :json)
