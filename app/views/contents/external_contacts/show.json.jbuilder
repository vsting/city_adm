# frozen_string_literal: true

json.partial! 'external_contacts/external_contact', external_contact: @external_contact
