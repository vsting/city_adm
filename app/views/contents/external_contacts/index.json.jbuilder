# frozen_string_literal: true

json.array! @external_contacts, partial: 'external_contacts/external_contact', as: :external_contact
