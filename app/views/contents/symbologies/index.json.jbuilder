# frozen_string_literal: true

json.array! @symbologies, partial: 'symbologies/symbology', as: :symbology
