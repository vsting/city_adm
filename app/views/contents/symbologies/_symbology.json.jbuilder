# frozen_string_literal: true

json.extract! symbology, :id, :title, :text, :created_at, :updated_at
json.url symbology_url(symbology, format: :json)
