# frozen_string_literal: true

json.partial! 'symbologies/symbology', symbology: @symbology
