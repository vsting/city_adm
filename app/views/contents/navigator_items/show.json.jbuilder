# frozen_string_literal: true

json.partial! 'navigator_items/navigator_item', navigator_item: @navigator_item
