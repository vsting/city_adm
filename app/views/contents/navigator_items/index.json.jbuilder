# frozen_string_literal: true

json.array! @navigator_items, partial: 'navigator_items/navigator_item', as: :navigator_item
