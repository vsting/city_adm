# frozen_string_literal: true

json.extract! navigator_item, :id, :title, :points, :created_at, :updated_at
json.url navigator_item_url(navigator_item, format: :json)
