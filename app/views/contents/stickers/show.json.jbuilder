# frozen_string_literal: true

json.partial! 'stickers/sticker', sticker: @sticker
