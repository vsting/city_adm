# frozen_string_literal: true

json.array! @stickers, partial: 'stickers/sticker', as: :sticker
