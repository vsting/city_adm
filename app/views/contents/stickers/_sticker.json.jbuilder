# frozen_string_literal: true

json.extract! sticker, :id, :title, :text, :stickerable_id, :stickerable_type, :created_at, :updated_at
json.url sticker_url(sticker, format: :json)
