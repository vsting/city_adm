# frozen_string_literal: true

json.partial! 'menu_items/menu_item', menu_item: @menu_item
