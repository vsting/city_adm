# frozen_string_literal: true

json.extract! menu_item, :id, :name, :title, :created_at, :updated_at
json.url menu_item_url(menu_item, format: :json)
