# frozen_string_literal: true

json.extract! for_settler, :id, :title, :text, :created_at, :updated_at
json.url for_settler_url(for_settler, format: :json)
