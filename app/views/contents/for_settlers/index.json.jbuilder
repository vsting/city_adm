# frozen_string_literal: true

json.array! @for_settlers, partial: 'for_settlers/for_settler', as: :for_settler
