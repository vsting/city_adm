# frozen_string_literal: true

json.partial! 'for_settlers/for_settler', for_settler: @for_settler
