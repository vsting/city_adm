# frozen_string_literal: true

json.array! @for_guests, partial: 'for_guests/for_guest', as: :for_guest
