# frozen_string_literal: true

json.extract! for_guest, :id, :title, :text, :created_at, :updated_at
json.url for_guest_url(for_guest, format: :json)
