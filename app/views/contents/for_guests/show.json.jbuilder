# frozen_string_literal: true

json.partial! 'for_guests/for_guest', for_guest: @for_guest
