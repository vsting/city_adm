# frozen_string_literal: true

json.partial! 'inspection_ratings/inspection_rating', inspection_rating: @inspection_rating
