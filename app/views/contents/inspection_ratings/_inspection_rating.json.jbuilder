# frozen_string_literal: true

json.extract! inspection_rating, :id, :title, :text, :created_at, :updated_at
json.url inspection_rating_url(inspection_rating, format: :json)
