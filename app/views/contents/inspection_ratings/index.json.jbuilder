# frozen_string_literal: true

json.array! @inspection_ratings, partial: 'inspection_ratings/inspection_rating', as: :inspection_rating
