# frozen_string_literal: true

json.extract! administration, :id, :title, :text, :created_at, :updated_at
json.url administration_url(administration, format: :json)
