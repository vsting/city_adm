# frozen_string_literal: true

json.partial! 'administrations/administration', administration: @administration
