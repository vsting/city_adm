# frozen_string_literal: true

json.extract! authority, :id, :title, :text, :created_at, :updated_at
json.url authority_url(authority, format: :json)
