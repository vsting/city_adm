# frozen_string_literal: true

json.partial! 'authorities/authority', authority: @authority
