# frozen_string_literal: true

json.array! @authorities, partial: 'authorities/authority', as: :authority
