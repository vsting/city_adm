# frozen_string_literal: true

json.extract! gallery, :id, :title, :text, :created_at, :updated_at
json.url gallery_url(gallery, format: :json)
