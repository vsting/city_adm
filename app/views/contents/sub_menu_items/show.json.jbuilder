# frozen_string_literal: true

json.partial! 'sub_menu_items/sub_menu_item', sub_menu_item: @sub_menu_item
