# frozen_string_literal: true

json.extract! sub_menu_item, :id, :menu_item_id, :name, :title, :created_at, :updated_at
json.url sub_menu_item_url(sub_menu_item, format: :json)
