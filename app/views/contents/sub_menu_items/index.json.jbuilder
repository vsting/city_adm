# frozen_string_literal: true

json.array! @sub_menu_items, partial: 'sub_menu_items/sub_menu_item', as: :sub_menu_item
