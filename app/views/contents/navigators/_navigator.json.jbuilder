# frozen_string_literal: true

json.extract! navigator, :id, :title, :text, :created_at, :updated_at
json.url navigator_url(navigator, format: :json)
