# frozen_string_literal: true

json.array! @navigators, partial: 'navigators/navigator', as: :navigator
