# frozen_string_literal: true

json.partial! 'navigators/navigator', navigator: @navigator
