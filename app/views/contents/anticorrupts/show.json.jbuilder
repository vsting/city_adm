# frozen_string_literal: true

json.partial! 'anticorrupts/anticorrupt', anticorrupt: @anticorrupt
