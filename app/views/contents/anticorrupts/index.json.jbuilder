# frozen_string_literal: true

json.array! @anticorrupts, partial: 'anticorrupts/anticorrupt', as: :anticorrupt
