# frozen_string_literal: true

json.extract! anticorrupt, :id, :name, :text, :created_at, :updated_at
json.url anticorrupt_url(anticorrupt, format: :json)
