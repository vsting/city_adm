# frozen_string_literal: true

json.extract! council, :id, :title, :text, :created_at, :updated_at
json.url council_url(council, format: :json)
