# frozen_string_literal: true

json.extract! inspection, :id, :name, :text, :created_at, :updated_at
json.url inspection_url(inspection, format: :json)
