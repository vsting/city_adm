# frozen_string_literal: true

class String
  def sql_clean(hash)
    case hash
      when :string
        self.gsub(/[.\/\\\]\[]|[%{}#'"*]|or|like|select|for|where|from|group/i, '')
      when :number
        self.gsub(/[^0-9]/, '')
    end
  end
end