# frozen_string_literal: true

class Date
  def self.ru_current_month_in_word
    I18n.t('date.formats.month_names')[Date.current.month]
  end

  def self.month_number(month_name)
    [nil, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'].index(month_name)
  end

  def self.to_ru_month(en_month_name)
    en_months_list = [nil, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    index = en_months_list.index(en_month_name)
    I18n.t('date.formats.month_names')[index]
  end
end
