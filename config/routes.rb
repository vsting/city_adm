# frozen_string_literal: true

Rails.application.routes.draw do
  resources :stickers
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # root 'landing#index'
  get '/tesla' => 'home#index'
  resources :feedback, only: :create

  root 'main#index'
  get 'index'  => 'main#index'
  get 'all'    => 'main#all'
  get '/today' => 'main#today'

  get 'download/file' # all types
  # resources :feedback, only: :create

  # match '/council',         :to => 'contents/synod#index',           via: :get, as: :council
  # match '/administrations', :to => 'contents/administrations#index', via: :get, as: :administration

  # namespace :contents, path: "", as: "" do
  # image, tag, attacheable_id, attacheable_type
  # has_many gallery

  # resources :subscription

  # resources :images

  get 'to_day/index', controller: 'contents/to_day'
  get 'to_day/show',  controller: 'contents/to_day'

  resources :users, only: %i[index show],                              controller: 'contents/users'
  resources :attachments,                                              controller: 'contents/attachments'

  resources :navigator,                                                controller: 'contents/navigators'
  resources :navigator_items,                                          controller: 'contents/navigator_items'
  resources :menu_items,                                               controller: 'contents/menu_items'
  resources :messages,                                                 controller: 'contents/messages' # , only: [:new, :create, :show, :edit, :update]                  #+

  get '/news',                          to: 'contents/news#index',     controller: 'contents/news', as: :news_index
  get '/news/one/:id',                  to: 'contents/news#show',      controller: 'contents/news', as: :news

  resources :authorities,               only: %i[index show],          controller: 'contents/authorities'
  resources :synods,                    only: :index, path: 'council', controller: 'contents/synods'
  resources :administrations,           only: :index,                  controller: 'contents/administrations'
  resources :documents,                 only: :index,                  controller: 'contents/documents'

  resources :galleries,                 only: %i[index show],          controller: 'contents/galleries'
  resources :posters,                   only: %i[index show],          controller: 'contents/posters'

  resources :abouts,                    only: :index,                  controller: 'contents/abouts'                                    # menu item
  resources :symbologies,               only: :index,                  controller: 'contents/symbologies'
  resources :histories,                 only: :index,                  controller: 'contents/histories'

  resources :officials,                 only: :index,                  controller: 'contents/officials'                                 #-
  resources :inspections,               only: :index,                  controller: 'contents/inspections'
  resources :inspection_ratings,        only: :index,                  controller: 'contents/inspection_ratings'
  resources :statistics,                only: :index,                  controller: 'contents/statistics'
  resources :external_contacts,         only: :index,                  controller: 'contents/external_contacts', path: 'ec/внешние связи города'
  resources :external_contact_contents, only: %i[index show],          controller: 'contents/external_contacts', path: 'ec/внешние связи города/контент'
  resources :anticorrupts,              only: %i[index show],          controller: 'contents/anticorrupts'
  resources :people,                    only: %i[index show],          controller: 'contents/people'
  resources :vacancies,                 only: %i[index show],          controller: 'contents/vacancies'

  resources :for_guests,                only: :index,                  controller: 'contents/for_guests' # link to :messages, :navigators
  resources :for_settlers,              only: :index,                  controller: 'contents/for_settlers' # link to :messages, :navigators

  get '/404', to: 'errors#error_404'
  get '/422', to: 'errors#error_422'
  get '/500', to: 'errors#error_500'
  get '/505', to: 'errors#error_505'

  match '/404' => 'errors#error_404', via: %i[get post patch delete]

  # You can have the root of your site routed with "root"

  # Верстка
  resources :mockups do
    get :index,
        :news,
        :news_one,
        :history,
        :symbols,
        :gallery,
        :gallery_one,
        :official,
        :message,
        :docs,
        :people,
        :afisha,
        :navigator,
        :gov,
        :text_page,
        on: :collection
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
