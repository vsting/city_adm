# frozen_string_literal: true

# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'ples'
set :repo_url, 'git@bitbucket.org:Kuzevanoff/ples.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
# Default deploy_to directory is /var/www/my_app_name

# Default value for :scm is :git
set :scm, :git
set :branch, 'master'

# set :passenger_in_gemfile, true
set :passenger_restart_with_touch, true
set :passenger_restart_with_sudo, true
set :passenger_port, 3002
# Default value for :format is :pretty
set :format, :pretty
set :rails_env, 'production'

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

set :linked_files, fetch(:linked_files, []).push('config/application.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

#  namespace :deploy do
#    after :restart, :clear_cache do
#      on roles(:web), in: :groups, limit: 3, wait: 10 do
#        # Here we can do anything such as:
#        within release_path do
#          execute :rake, 'db:reset'
#          execute :rake, 'db:migrate'
#          execute :rake, 'db:seed'
#        end
#      end
#    end
#  end

# namespace :db do
#   task :all_seeds do
#     on roles(:all) do
#       # Here we can do anything such as:
#       within release_path do
#         execute :rake, 'db:seed RAILS_ENV="development"'
#       end
#     end
#   end
# end

# DaeSzXrwjeEgqDFn

# for run: cap staging db:seed:update_platform_specific_info
namespace :db do
  namespace :seed do
    task :all_seeds do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:seed'
            # execute :rake, 'db:seed:rostov_social_accounts'
          end
        end
      end
    end

    task :task_prepare do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:prepare'
          end
        end
      end
    end

    task :task_reset do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:reset'
          end
        end
      end
    end

    task :task_migrate do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:migrate'
          end
        end
      end
    end
  end
end
