
## Install

```bash
rake db:create
rake db:migrate
rake db:seed
```

## Check code style

```bash
rubocop -a
```