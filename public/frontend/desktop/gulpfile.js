"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const debug = require("gulp-debug");
const sourcemaps = require("gulp-sourcemaps");
const gulpIf = require("gulp-if");
const notify = require("gulp-notify");
const combiner = require("stream-combiner2").obj;
const del = require("del");
const svgSprite = require("gulp-svg-sprite");
const spritesmith = require('gulp.spritesmith');
const rev = require("gulp-rev");
const gzip = require("gulp-gzip");

//PostCSS
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require('cssnano');


const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == "development";

gulp.task("styles", function () {
    let processors = [
        autoprefixer
    ];

    if (!isDevelopment) {
        processors.push(cssnano);
    }

    return combiner(gulp.src("source/styles/app.sass"),
        gulpIf(isDevelopment, sourcemaps.init()),
        sass(),
        postcss(processors),
        gulpIf(isDevelopment, sourcemaps.write()),
        rev(),
        gulp.dest("build/styles"),
        rev.manifest("styles.json"),
        gulp.dest("build/styles")
    ).on("error", notify.onError());
});

gulp.task("styles:svg", function () {
    return gulp.src("source/img/svg/**/*.*")
        .pipe(svgSprite({
            mode: {
                css: {
                    dest: ".",
                    sprite: "sprite.svg",
                    layout: "vertical",
                    dimensions: true,
                    render: {
                        scss: {
                            dest: "_sprite.scss"
                        }
                    }
                }
            }
        }))
        .pipe(debug({title: "styles:svg"}))
        .pipe(gulpIf("*.scss", gulp.dest("source/styles/generated"), gulp.dest("build/styles")));
});

//spritesmith не обладает опцией хэша для имени файла. Поэтому будем добавлять свой уникальный
//кусок имени. Пока поступим просто - возьмем текущее время.
let randParam = +(new Date());
gulp.task("styles:png", function () {
    return gulp.src("source/img/png/**/*.*")
        .pipe(spritesmith({
            imgName: `sprite_${randParam}.png`,
            cssName: 'sprite_png.css',
            padding: 15
        }))
        .pipe(debug({title: "styles:png"}))
        .pipe(gulpIf("*.css", gulp.dest("source/styles/generated"), gulp.dest("build/styles")));
});

gulp.task("assets:img", function () {
    return gulp.src("source/img/**/*.*", {since: gulp.lastRun("assets:img")})
        .pipe(gulp.dest("build/img"));

});

gulp.task("clean", function () {
    return del(["build/img", "build/styles"]);
});

gulp.task("gzip", function (callback) {
    //Сжатие нужно нам только в продакшн режиме
    if(isDevelopment) {
        callback();
        return;
    }

    return gulp.src("build/styles/*")
        .pipe(gzip())
        .pipe(gulp.dest("build/styles"));
});

gulp.task("build", gulp.series(
    "clean",
    //styles:svg и styles:png должно быть до styles, т.к. styles:svg/png генерирует файл, который используем styles
    "styles:svg",
    "styles:png",
    "styles",
    "assets:img",
    //gzip должен быть вконце - все файлы уже должны быть на месте
    "gzip"
));

gulp.task("watch", function () {
    gulp.watch("source/styles/**/*.*", gulp.series("styles"));
    gulp.watch("source/img/**/*.*", gulp.series("assets:img"));
    gulp.watch("source/img/svg/**/*.*", gulp.series("styles:svg"));
    gulp.watch("source/img/png/**/*.*", gulp.series("styles:png"));
});

gulp.task("dev", gulp.series("build", "watch"));